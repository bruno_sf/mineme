HOT means a special feature which plays a big impact on the plugin.
Ideas:
1.8
TODO - Add mesh Shaped Mine (Polyhedral)
TODO - Add holograms customization, presets,
1.7
TODO - Add region support, includes auto region creation, group management, etc
TODO - Add database support
- Now uses Shiroi as the plugin framework
- Fixed a bug introduced in 1.9 where if you clicked on the upper on lower inventory in the gui, the menu would actually as if they were the same (This was actually fixed in the Shiroi Framework)
- Menus are now scrollable
1.6
# Done
- Removed async plugin loading, uneffective
- Able to load mine files from MEGUI [HOT]
- Fixed that HORRIBLE reseted grammar mistake, omg kill me plz
- Code optimization
- Composition Editor Menu has a new layout and displays a lot more information [HOT]
- MineMe now uses a new Inventory Menu API that I've been developing lately, it's much faster, such wow, changes include: [SUPER HOT]
  [*] Complete UI Remake
  [*] Things may look a bit different, added more items, changed others, moved a few things here and there, etc
  [*] Able modify Mine Effects from MEGUI with
  [*] Able to load unloaded mine files from MEGUI's MainMenu
  [*] 5 new buttons with new functionalities to the Mine Menu[Clear Mine, Fill Mine, Balance materials, Reset materials, Disable mine]
-  Added Kuri