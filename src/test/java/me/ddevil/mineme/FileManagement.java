package me.ddevil.mineme;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.Base64;

/**
 * Created by BRUNO II on 01/07/2016.
 */
public class FileManagement {

    private static File folder = new File("C:\\Users\\BRUNO II\\Dropbox\\localdev\\server\\plugins\\MineMe\\storage");
    private static String name = "examplepoligonalmine";

    public static void main(String[] args) {
        File minestoragefile = new File(folder, name + ".mineme");
        if (!minestoragefile.exists()) {
            try {
                minestoragefile = createNewMineFile(name);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileReader fileReader = new FileReader(minestoragefile);
            String encoded = "";
            while (fileReader.ready()) {
                encoded += (char) fileReader.read();
            }
            System.out.println("encoded string = " + encoded);
            JSONObject json = (JSONObject) new JSONParser().parse(new String(Base64.getDecoder().decode(encoded)));
            System.out.println(json
                    .toJSONString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static File createNewMineFile(String name) throws IOException {
        JSONObject json = new JSONObject();
        json.put("mine", name);
        json.put("totalbrokenblocks", 0);
        json.put("totaltimesreseted", 0);
        File storagefile = new File(folder, name + ".mineme");
        try (FileWriter fileWriter = new FileWriter(storagefile)) {
            fileWriter.write(Base64.getEncoder().encodeToString(json.toJSONString().getBytes()));
        }
        return storagefile;
    }
}
