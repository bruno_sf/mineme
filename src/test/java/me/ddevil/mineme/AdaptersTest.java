package me.ddevil.mineme;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by BRUNO II on 06/07/2016.
 */
public class AdaptersTest {
    public static void main(String[] args) {
        String input = "&2Emerald $2percentage$1 %composition:EMERALD_ORE:2%";
        Matcher m = COMPOSITION_PATTERN.matcher(input);
        boolean matches = m.find();
        System.out.println(m.group(1));
        System.out.println(matches);
        System.out.println(m.groupCount());
    }

    public static Pattern COMPOSITION_PATTERN = Pattern.compile("%composition:([\\w:]+)%", Pattern.CASE_INSENSITIVE|Pattern.MULTILINE);
}
