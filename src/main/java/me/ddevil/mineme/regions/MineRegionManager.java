package me.ddevil.mineme.regions;

import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.utils.misc.Toggleable;

/**
 * Created by BRUNO II on 05/07/2016.
 */
public interface MineRegionManager<T extends MineRegionManager, R extends MineRegion> extends Toggleable {

    R getRegion(Mine mine);

    RegionAdapterType getAdapterType();
}
