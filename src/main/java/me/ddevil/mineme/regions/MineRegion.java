package me.ddevil.mineme.regions;

import me.ddevil.mineme.mines.Mine;

import java.util.List;

/**
 * Created by BRUNO II on 05/07/2016.
 */
public interface MineRegion {
    Mine getMine();

    boolean isGroupAllowed(String groupName);

    List<String> getAllowedGroups();
}
