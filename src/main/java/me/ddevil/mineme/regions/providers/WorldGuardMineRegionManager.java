package me.ddevil.mineme.regions.providers;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.regions.selector.Polygonal2DRegionSelector;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.types.CircularMine;
import me.ddevil.mineme.mines.types.CuboidMine;
import me.ddevil.mineme.mines.types.PolygonMine;
import me.ddevil.mineme.regions.BaseMineRegionManager;
import me.ddevil.mineme.regions.MineRegion;
import me.ddevil.mineme.regions.MineRegionManager;
import me.ddevil.mineme.regions.RegionAdapterType;
import org.bukkit.Bukkit;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by BRUNO II on 05/07/2016.
 */
public class WorldGuardMineRegionManager extends BaseMineRegionManager<WorldGuardMineRegionManager, WorldGuardMineRegion> {

    private final String regionPrefix;
    private final WorldGuardPlugin worldGuardPlugin;
    private final Map<Mine, WorldGuardMineRegion> regions;

    public WorldGuardMineRegionManager(MineMe mineMe) {
        super(mineMe);
        worldGuardPlugin = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");
        regionPrefix = plugin.getConfigurationManager().mineRegionPrefix();
        regions = new HashMap();
    }


    public WorldGuardMineRegion getRegion(Mine mine) {
        if (regions.containsKey(mine)) {
            return regions.get(mine);
        }
        RegionManager mineRegionManager = worldGuardPlugin.getRegionManager(mine.getWorld());
        String name = regionPrefix + mine.getName();
        ProtectedRegion region;
        if (!mineRegionManager.hasRegion(name)) {
            if (mine instanceof CuboidMine) {
                CuboidMine cuboidMine = (CuboidMine) mine;
                Vector pos1 = cuboidMine.getPos1();
                Vector pos2 = cuboidMine.getPos2();
                region = new ProtectedCuboidRegion(
                        name,
                        new BlockVector(pos1.getX(), pos1.getY(), pos1.getZ()),
                        new BlockVector(pos2.getX(), pos2.getY(), pos2.getZ())
                );
            } else if (mine instanceof CircularMine) {
                CircularMine circularMine = (CircularMine) mine;
                region = new ProtectedPolygonalRegion(name, mine.getWERegion().polygonize(Integer.MAX_VALUE), circularMine.getLowerY(), circularMine.getUpperY());
            } else if (mine instanceof PolygonMine) {
                PolygonMine polygonalMine = (PolygonMine) mine;
                region = new ProtectedPolygonalRegion(name, polygonalMine.getWERegion().getPoints(), polygonalMine.getLowerY(), polygonalMine.getUpperY());
            } else {
                throw new IllegalArgumentException("Can't get region for mine " + mine.getName() + " (" + mine.getClass().getSimpleName() + ")!");
            }
        } else {
            region = mineRegionManager.getRegion(name);
        }
        WorldGuardMineRegion mineRegion = new WorldGuardMineRegion(plugin, region, mine);
        regions.put(mine, mineRegion);
        return mineRegion;
    }

    @Override
    public RegionAdapterType getAdapterType() {
        return RegionAdapterType.WORLD_GUARD;
    }

    @Override
    public WorldGuardMineRegionManager setup() {
        return this;
    }
}
