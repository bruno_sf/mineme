package me.ddevil.mineme.regions.providers;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.regions.BaseMineRegion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BRUNO II on 06/07/2016.
 */
public class WorldGuardMineRegion extends BaseMineRegion {
    private final ProtectedRegion region;

    public WorldGuardMineRegion(MineMe plugin, ProtectedRegion region, Mine mine) {
        super(plugin, mine);
        WorldGuardMineRegionManager regionManager = (WorldGuardMineRegionManager) plugin.getRegionManager();
        this.region = region;
    }

    @Override
    public boolean isGroupAllowed(String groupName) {
        return region.getMembers().getGroupDomain().contains(groupName);
    }

    @Override
    public List<String> getAllowedGroups() {
        return new ArrayList<>(region.getMembers().getGroups());
    }
}
