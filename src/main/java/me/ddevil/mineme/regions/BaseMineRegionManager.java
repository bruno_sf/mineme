package me.ddevil.mineme.regions;

import me.ddevil.mineme.MineMe;
import me.ddevil.shiroi.misc.BaseManager;

/**
 * Created by BRUNO II on 10/07/2016.
 */
public abstract class BaseMineRegionManager<T extends BaseMineRegionManager, R extends MineRegion> extends BaseManager<MineMe, T> implements MineRegionManager<T,R> {

    public BaseMineRegionManager(MineMe plugin) {
        super(plugin);
    }

}
