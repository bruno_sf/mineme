package me.ddevil.mineme.regions;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BRUNO II on 06/07/2016.
 */
public abstract class BaseMineRegion implements MineRegion {
    protected final Mine mine;
    protected final MineMe plugin;

    public BaseMineRegion(MineMe plugin, Mine mine) {
        this.mine = mine;
        this.plugin = plugin;
    }

    @Override
    public Mine getMine() {
        return mine;
    }

}
