/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.placeholders;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineManager;
import me.ddevil.mineme.placeholders.managers.MVdWHandler;
import me.ddevil.mineme.placeholders.managers.PlaceholderAPIHandler;
import me.ddevil.mineme.placeholders.managers.PlaceholderHandler;
import me.ddevil.shiroi.config.ConstantsConfigurationManager;
import me.ddevil.shiroi.misc.BukkitToggleable;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

/**
 * @author DDevil_
 */
public class PlaceholderManager extends BukkitToggleable<MineMe> {

    private static final String MVDW_PLACEHOLDER_API = "MVdWPlaceholderAPI";
    private static final String PLACEHOLDER_API = "PlaceholderAPI";
    private final ConstantsConfigurationManager<?> configurationManager;

    public boolean isPlaceholderRegistered(Mine m, PlaceholderProvider placeholderProvider) {
        switch (placeholderProvider) {
            case MVDW:
                return mvdwManager.isPlaceholderRegistered(m);
            case PLACEHOLDER_API:
                return placeholderAPIManager.isPlaceholderRegistered(m);
            default:
                return false;
        }
    }

    public void registerMinePlaceholders(Mine m, PlaceholderProvider provider) {
        switch (provider) {
            case MVDW:
                mvdwManager.registerMinePlaceholders(m);
                break;
            case PLACEHOLDER_API:
                placeholderAPIManager.registerMinePlaceholders(m);
                break;
        }
    }

    public boolean isUsable(PlaceholderProvider placeholderProvider) {
        return getHandler(placeholderProvider) != null;
    }

    private PlaceholderHandler getHandler(PlaceholderProvider placeholderProvider) {
        switch (placeholderProvider) {
            case MVDW:
                return mvdwManager;
            case PLACEHOLDER_API:
                return placeholderAPIManager;
            default:
                return null;
        }
    }

    public enum PlaceholderProvider {
        MVDW, PLACEHOLDER_API;
    }

    private final MineManager mineManager;
    private MVdWHandler mvdwManager;
    private PlaceholderAPIHandler placeholderAPIManager;

    public PlaceholderManager(MineMe plugin) {
        super(plugin);
        this.configurationManager = plugin.getConfigManager();
        this.mineManager = plugin.getMineManager();
    }

    public void setup() {
        PluginManager pm = Bukkit.getPluginManager();
        if (pm.isPluginEnabled(MVDW_PLACEHOLDER_API)) {
            this.mvdwManager = new MVdWHandler(plugin);
            mvdwManager.setup();
        }
        if (pm.isPluginEnabled(PLACEHOLDER_API)) {
            this.placeholderAPIManager = new PlaceholderAPIHandler(plugin);
            placeholderAPIManager.setup();
        }
    }


}
