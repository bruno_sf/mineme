package me.ddevil.mineme.placeholders.managers;

import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.utils.misc.Toggleable;

/**
 * Created by BRUNO II on 01/07/2016.
 */
public interface PlaceholderHandler extends Toggleable {
    public void registerMinePlaceholders(final Mine m);

    public boolean isPlaceholderRegistered(Mine m);
}
