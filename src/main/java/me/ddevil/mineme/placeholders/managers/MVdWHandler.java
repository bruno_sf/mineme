package me.ddevil.mineme.placeholders.managers;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineManager;
import me.ddevil.mineme.placeholders.MineMePlaceholder;
import me.ddevil.mineme.placeholders.PlaceholderRequest;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by BRUNO II on 01/07/2016.
 */
public class MVdWHandler extends BasePlaceholderHandler {
    private final MineManager mineManager;

    public MVdWHandler(MineMe plugin) {
        super(plugin);
        this.mineManager = plugin.getMineManager();
    }

    @Override
    public void setup() {
        plugin.debug("Registering MVdW placeholders...", true);
        //Global placeholders
        for (MineMePlaceholder mineMePlaceholder : MineMePlaceholder.values()) {
            PlaceholderAPI.registerPlaceholder(plugin, MineMePlaceholder.PLACEHOLDER_PREFIX + MineMePlaceholder.CURRENT_MINE_MODIFIER + ":" + mineMePlaceholder.getReplacer(), e -> {
                Player p = e.getPlayer();
                return mineMePlaceholder.response(new PlaceholderRequest(p, mineManager.getMineWith(p)));
            });
        }
        //Mine placeholders
        mineManager.getMines().forEach(this::registerMinePlaceholders);
        plugin.debug("Placeholders registered!", true);
    }

    private final ArrayList<Mine> registeredMines = new ArrayList();

    public void registerMinePlaceholders(final Mine m) {
        if (!registeredMines.contains(m)) {
            for (MineMePlaceholder placeholder : MineMePlaceholder.values()) {
                PlaceholderAPI.registerPlaceholder(plugin, MineMePlaceholder.PLACEHOLDER_PREFIX + m.getName() + ":" + placeholder.getReplacer(), e -> {
                    Player p = e.getPlayer();
                    return placeholder.response(new PlaceholderRequest(p, mineManager.getMineWith(p)));
                });
            }
            registeredMines.add(m);
        }
    }

    public boolean isPlaceholderRegistered(Mine m) {
        return registeredMines.contains(m);
    }

}
