package me.ddevil.mineme.placeholders.managers;

import me.ddevil.mineme.MineMe;
import me.ddevil.shiroi.misc.BukkitToggleable;

/**
 * Created by BRUNO II on 12/07/2016.
 */
public abstract class BasePlaceholderHandler extends BukkitToggleable<MineMe> implements PlaceholderHandler {
    public BasePlaceholderHandler(MineMe plugin) {
        super(plugin);
    }
}
