package me.ddevil.mineme.placeholders.managers;
import me.clip.placeholderapi.PlaceholderAPI;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;

/**
 * Created by BRUNO II on 01/07/2016.
 */
public class PlaceholderAPIHandler extends BasePlaceholderHandler {
    public PlaceholderAPIHandler(MineMe plugin) {
        super(plugin);
    }

    @Override
    public void setup() {
        PlaceholderAPI.registerPlaceholderHook(plugin, new MineMeHook(plugin));
    }

    @Override
    public void registerMinePlaceholders(Mine m) {

    }

    @Override
    public boolean isPlaceholderRegistered(Mine m) {
        return false;
    }

}

