package me.ddevil.mineme.placeholders.managers;

import me.clip.placeholderapi.PlaceholderHook;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineManager;
import me.ddevil.mineme.placeholders.MineMePlaceholder;
import me.ddevil.mineme.placeholders.PlaceholderRequest;
import org.bukkit.entity.Player;

/**
 * Created by bruno on 15/09/2016.
 */
public class MineMeHook extends PlaceholderHook {
    private final MineMe plugin;
    private final MineManager mineManager;

    MineMeHook(MineMe plugin) {
        this.plugin = plugin;
        this.mineManager = plugin.getMineManager();
    }

    @Override
    public String onPlaceholderRequest(Player player, String s) {
        String[] split = s.split(":");
        if (split.length < 2) {
            plugin.debug("Placeholder " + s + " is badly configured! should be: '" + MineMePlaceholder.PLACEHOLDER_PREFIX + ":{mine modifier here}:{placeholder}", true);
            return "Error, check console!";
        }
        MineMePlaceholder placeholder = MineMePlaceholder.forString(s);
        if (placeholder == null) {
            plugin.debug("Could not find placeholder for string " + s, true);
            return "Error, check console!";
        }
        String mine = split[1];
        Mine m;
        if (mine.equalsIgnoreCase(MineMePlaceholder.CURRENT_MINE_MODIFIER)) {
            m = mineManager.getMineWith(player);

        } else {
            m = mineManager.getMine(mine);
        }
        if (m == null) {
            plugin.debug("Could not find mine with name" + mine, true);
            return "Error, check console!";
        }
        return placeholder.response(new PlaceholderRequest(player, m));
    }
}
