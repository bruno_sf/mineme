package me.ddevil.mineme.placeholders;

import me.ddevil.mineme.challenges.Challenge;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.utils.StringUtils;
import org.bukkit.entity.Player;

/**
 * Format = {@link #PLACEHOLDER_PREFIX}:{@link #CURRENT_MINE_MODIFIER}:{@link #getReplacer()}
 * mineme:current:remaining
 */
public enum MineMePlaceholder {


    NAME("name") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine != null ? new String() : mine.getName();
        }
    },

    ALIAS("alias") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine != null ? new String() : mine.getAlias();
        }
    },

    REMAINING_BLOCKS("remaining") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine != null ? new String() : String.valueOf(mine.getRemainingBlocks());
        }
    },

    MINED_BLOCKS("mined") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine != null ? new String() : String.valueOf(mine.getMinedBlocks());
        }
    },

    MINED_BLOCKS_PERCENTAGE("minedpercent") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine == null ? new String() : String.valueOf(mine.getPercentageMined());
        }
    },

    REMAINING_BLOCKS_PERCENTAGE("remainingpercent") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine != null ? new String() : String.valueOf(mine.getPercentageRemaining());
        }
    },

    AVERAGE_SPEED("avgspeed") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine != null ? new String() : String.valueOf(mine.getRemainingBlocks());
        }
    },

    RESET_TIME("resettime") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine == null ? new String() : StringUtils.secondsToString(mine.getResetTime());

        }
    },

    NEXT_RESET_TIME("nextresettime") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            return mine == null ? new String() : StringUtils.secondsToString(mine.getTimeToNextReset());

        }
    },

    LIFETIME_BROKEN_BLOCKS("totalbrokenblocks") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            try {
                return mine == null ? new String() : String.valueOf(mine.getLifetimeBrokenBlocks());
            } catch (Exception e) {
                return "error " + e.getClass().getSimpleName();
            }
        }
    },

    PLAYER_LIFETIME_BROKEN_BLOCKS("playerbrokenblocks") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Mine mine = placeholderRequest.getMine();
            Player player = placeholderRequest.getPlayer();
            if (player == null) {
                return new String();
            }
            return mine == null ? new String() : String.valueOf(mine.getTotalBlocksBroken(player));
        }
    },
    //Challenges
    CHALLENGE_NAME("challenge:name", false) {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Challenge currentChallenge = placeholderRequest.getChallenge();
            if (currentChallenge != null) {
                return currentChallenge.getName();
            }
            return new String();
        }
    },
    CHALLENGE_ALIAS("challenge:alias") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Challenge currentChallenge = placeholderRequest.getChallenge();
            if (currentChallenge != null) {
                return currentChallenge.getAlias();
            }
            return new String();
        }
    },
    CHALLENGE_TIME("challenge:time") {
        @Override
        public String response(PlaceholderRequest placeholderRequest) {
            Challenge currentChallenge = placeholderRequest.getChallenge();
            if (currentChallenge != null) {
                return StringUtils.secondsToString(currentChallenge.getTimeRemaining());
            }
            return new String();
        }
    };
    public static final String PLACEHOLDER_PREFIX = "mineme:";
    public static final String CURRENT_MINE_MODIFIER = "current";
    private final boolean requireChallenge;

    public static MineMePlaceholder forString(String entry) {
        String[] split = entry.split(":");
        if (split.length < 2) {
            return null;
        }
        String name = split[2];
        for (MineMePlaceholder placeholder : values()) {
            if (placeholder.getReplacer().equalsIgnoreCase(name)) {
                return placeholder;
            }
        }
        return null;
    }

    private final String replacer;

    MineMePlaceholder(String replacer) {
        this(replacer, false);
    }

    MineMePlaceholder(String replacer, boolean requireChallenge) {
        this.replacer = replacer;
        this.requireChallenge = requireChallenge;
    }


    public abstract String response(PlaceholderRequest placeholderRequest);

    public boolean requireChallenge() {
        return requireChallenge;
    }

    public String getReplacer() {
        return replacer;
    }
}
