package me.ddevil.mineme.placeholders;

import me.ddevil.mineme.challenges.Challenge;
import me.ddevil.mineme.mines.Mine;
import org.bukkit.entity.Player;

/**
 * Created by BRUNO II on 30/06/2016.
 */
public class PlaceholderRequest {
    private final Player player;
    private final Mine mine;
    private final Challenge challenge;

    public PlaceholderRequest(Player player, Mine mine, Challenge challenge) {
        this.player = player;
        this.mine = mine;
        this.challenge = challenge;
    }

    public PlaceholderRequest(Player player, Mine mine) {
        this.player = player;
        this.mine = mine;
        this.challenge = mine.getCurrentChallenge();
    }

    public Player getPlayer() {
        return player;
    }

    public Mine getMine() {
        return mine;
    }

    public Challenge getChallenge() {
        return challenge;
    }
}
