package me.ddevil.mineme.challenges.providers;

import me.ddevil.mineme.challenges.ChallengeParameters;
import me.ddevil.mineme.mines.Mine;

/**
 * Created by BRUNO II on 13/07/2016.
 */
public final class ChallengeRequest {
    private final Mine mine;
    private final ChallengeParameters challengeParameters;

    public ChallengeRequest(Mine mine, ChallengeParameters challengeParameters) {
        this.mine = mine;
        this.challengeParameters = challengeParameters;
    }

    public Mine getMine() {
        return mine;
    }


}
