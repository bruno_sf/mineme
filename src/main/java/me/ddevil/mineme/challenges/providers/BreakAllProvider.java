package me.ddevil.mineme.challenges.providers;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.types.BreakAllChallenge;

/**
 * Created by BRUNO II on 13/07/2016.
 */
public class BreakAllProvider implements ChallengeProvider<BreakAllChallenge> {
    private final MineMe plugin;

    public BreakAllProvider(MineMe plugin) {
        this.plugin = plugin;
    }

    @Override
    public BreakAllChallenge provide(ChallengeRequest requester) {
        return new BreakAllChallenge(plugin, requester.getMine(), 3);
    }
}
