package me.ddevil.mineme.challenges.providers;

import me.ddevil.mineme.challenges.Challenge;

/**
 * Created by BRUNO II on 12/07/2016.
 */
public interface ChallengeProvider<C extends Challenge> {
    C provide(ChallengeRequest requester);
}
