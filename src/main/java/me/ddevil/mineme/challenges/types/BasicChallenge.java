/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.challenges.types;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.Challenge;
import me.ddevil.mineme.challenges.ChallengeEndListener;
import me.ddevil.mineme.challenges.ChallengeResult;
import me.ddevil.mineme.challenges.ChallengeResultType;
import me.ddevil.mineme.messages.MineMeMessageManager;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DDevil_
 */
public abstract class BasicChallenge implements Challenge {

    protected final String name;
    protected final MineMe plugin;
    protected final String alias;
    protected final int totalTime;
    private final boolean requiresEconomy;
    protected int timeLeft;
    protected BukkitTask countdownTask;
    protected final List<Player> players;

    public BasicChallenge(MineMe plugin, String name, String alias, int totalTime, boolean requiresEconomy) {
        if(requiresEconomy && plugin.getEconomyManager() == null){
            throw new IllegalArgumentException("This challenge requires an economy plugin but there is none installed!");
        }
        this.plugin = plugin;
        this.name = name;
        this.alias = alias;
        this.totalTime = totalTime;
        this.timeLeft = totalTime;
        this.players = new ArrayList();
        this.requiresEconomy = requiresEconomy;
    }

    @Override
    public String getName() {
        return name;
    }

    protected final ArrayList<ChallengeEndListener> listeners = new ArrayList();

    @Override
    public void start() {
        this.countdownTask = new BukkitRunnable() {

            @Override
            public void run() {
                timeLeft -= 1;
            }
        }.runTaskTimer(plugin, 20l, 20l);
        doStart();
    }

    protected abstract void doStart();

    @Override
    public void addListener(ChallengeEndListener listener) {
        listeners.add(listener);
    }

    @Override
    public void complete(ChallengeResultType result, List<Player> winners) {
        handleComplete(new ChallengeResult(result, winners));
        for (ChallengeEndListener listener : listeners) {
            listener.onComplete(result);
        }
    }

    @Override
    public String getAlias() {
        return alias;
    }

    protected abstract void handleComplete(ChallengeResult result);

    @Override
    public int getTotalTime() {
        return totalTime;
    }

    @Override
    public int getTimeRemaining() {
        return timeLeft;
    }

    @Override
    public List<Player> getPlayers() {
        return players;
    }

    @Override
    public void addPlayer(Player player) {
        if (!players.contains(player)) {
            MineMeMessageManager messageManager = plugin.getMessageManager();
            messageManager.sendMessage(player, messageManager.translateAll(plugin
            .getConfigurationManager().getChallengeJoin(), this));
            players.add(player);
        }
    }

    @Override
    public void removePlayer(Player player) {
        if (players.contains(player)) {
            MineMeMessageManager messageManager = plugin.getMessageManager();
            messageManager.sendMessage(player, messageManager.translateAll(plugin
                    .getConfigurationManager().getChallengeKick(), this));
            players.remove(player);
        }
    }

    @Override
    public boolean requiresEconomy() {
        return requiresEconomy;
    }
}
