package me.ddevil.mineme.challenges.types;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.ChallengeResult;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by BRUNO II on 05/08/2016.
 */
public class SpeedRunChallenge extends BasicChallenge {


    public SpeedRunChallenge(MineMe plugin, String name, String alias, int totalTime) {
        super(plugin, name, alias, totalTime, true);
    }

    @Override
    protected void handleComplete(ChallengeResult result) {

    }

    @Override
    public List<Player> getPlayers() {
        return null;
    }

    @Override
    public void start() {

    }

    @Override
    protected void doStart() {

    }
}
