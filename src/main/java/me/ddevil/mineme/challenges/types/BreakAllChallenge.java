/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.challenges.types;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.ChallengeResult;
import me.ddevil.mineme.events.mines.MineBlockBreakEvent;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.effects.FireworkExplosion;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.scheduler.BukkitTask;

import java.util.List;

/**
 * @author DDevil_
 */
public class BreakAllChallenge extends BasicChallenge {

    protected final Mine mine;
    protected final int timeLimit;
    protected int timeLeft;
    private BukkitTask countdown;

    public BreakAllChallenge(MineMe plugin, Mine mine, int timeLimit) {
        super(plugin, "frenzy", plugin.getConfigurationManager().getChallengeAlias("frenzy"), timeLimit, true);
        this.mine = mine;
        this.timeLimit = timeLimit;
        this.timeLeft = timeLimit;
    }


    @Override
    protected void doStart() {
        plugin.registerListener(this);
    }

    @EventHandler
    public void blockBreakEvent(MineBlockBreakEvent event) {
        if (event.getMine().equals(mine)) {
            if (!event.isCancelled()) {
                Player player = event.getPlayer();
                if (!players.contains(player)) {
                    addPlayer(player);
                }
            }
        }
    }


    @Override
    protected void handleComplete(ChallengeResult result) {
        switch (result.getType()) {
            case COMPLETED:
                List<Player> winners = result.getWinners();
                for (Player winner : winners) {
                    new FireworkExplosion(winner.getLocation().add(0, 0.5, 0), plugin).explode();
                }
                break;
            case FAILED:
                break;
        }
    }
}
