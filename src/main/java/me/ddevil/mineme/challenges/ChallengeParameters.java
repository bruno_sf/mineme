package me.ddevil.mineme.challenges;

import me.ddevil.mineme.mines.Mine;

/**
 * Created by BRUNO II on 13/07/2016.
 */
public class ChallengeParameters {
    private final String challengeName;
    private final int challengeDuration;
    private final int challengeAmplifier;
    private final Mine mine;

    public ChallengeParameters(Mine mine, String challengeName, int challengeDuration, int challengeAmplifier) {
        this.mine = mine;
        this.challengeName = challengeName;
        this.challengeDuration = challengeDuration;
        this.challengeAmplifier = challengeAmplifier;
    }

    public String getChallengeName() {
        return challengeName;
    }

    public int getChallengeDuration() {
        return challengeDuration;
    }

    public int getChallengeAmplifier() {
        return challengeAmplifier;
    }


    public Mine getMine() {
        return mine;
    }
}
