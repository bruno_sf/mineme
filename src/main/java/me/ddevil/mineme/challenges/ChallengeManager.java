/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.challenges;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.providers.BreakAllProvider;
import me.ddevil.mineme.challenges.providers.ChallengeProvider;
import me.ddevil.mineme.challenges.providers.ChallengeRequest;
import me.ddevil.shiroi.misc.BukkitToggleable;
import me.ddevil.shiroi.misc.DebugLevel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author DDevil_
 */
public class ChallengeManager extends BukkitToggleable<MineMe> {
    private final Map<String, ChallengeProvider> challengeProviders = new HashMap();

    public ChallengeManager(MineMe plugin) {
        super(plugin);
    }

    @Override
    public void setup() {
        super.setup();
        registerProvider("frenzy", new BreakAllProvider(plugin));
    }

    public void registerProvider(String challengeName, ChallengeProvider provider) {
        challengeProviders.put(challengeName, provider);
        plugin.debug("Registered provider " + provider.getClass().getName() + "!", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
    }

    public Challenge requestChallenge(ChallengeParameters challengeParameters) throws IllegalArgumentException {
        ChallengeProvider provider = challengeProviders.get(challengeParameters.getChallengeName());
        if (provider == null) {
            throw new IllegalArgumentException("There is no provider for challenge " + challengeParameters.getChallengeName());
        }
        return provider.provide(new ChallengeRequest(challengeParameters.getMine(), challengeParameters));
    }
}
