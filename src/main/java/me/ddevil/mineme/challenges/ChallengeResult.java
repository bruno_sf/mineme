package me.ddevil.mineme.challenges;

import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by BRUNO II on 05/08/2016.
 */
public class ChallengeResult {
    private final ChallengeResultType type;
    private final List<Player> winners;

    public ChallengeResult(ChallengeResultType type, List<Player> winners) {
        this.type = type;
        this.winners = winners;
    }

    public ChallengeResultType getType() {
        return type;
    }

    public List<Player> getWinners() {
        return winners;
    }
}
