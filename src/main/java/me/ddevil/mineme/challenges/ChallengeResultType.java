package me.ddevil.mineme.challenges;

/**
 * Created by BRUNO II on 12/07/2016.
 */
public enum ChallengeResultType {
    COMPLETED,
    FAILED;
}
