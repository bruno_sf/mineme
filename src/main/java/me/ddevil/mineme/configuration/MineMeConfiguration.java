package me.ddevil.mineme.configuration;

import me.ddevil.shiroi.config.BaseConfigurationValue;
import org.bukkit.configuration.ConfigurationSection;
import java.util.List;

public class MineMeConfiguration<T> extends BaseConfigurationValue<T> {
    //<editor-fold desc="config.yml">
    //<editor-fold desc="General">
    public static final MineMeConfiguration<String> CONFIG_VERSION = new MineMeConfiguration<>("configVersion", String.class);
    public static final MineMeConfiguration<Boolean> CONVERT_FROM_MRL = new MineMeConfiguration<>("global.convertFromMineResetLite", Boolean.class);
    public static final MineMeConfiguration<Boolean> FORCE_DEFAULT_BROADCAST_MESSAGE = new MineMeConfiguration<>("global.forceDefaultBroadcastMessage", Boolean.class);
    public static final MineMeConfiguration<Integer> MINIMUM_DEBUG_LEVEL = new MineMeConfiguration<>("settings.minimumDebugLevel", Integer.class);
    public static final MineMeConfiguration<Boolean> SHOW_LOVE_TO_DEVELOPER = new MineMeConfiguration<>("settings.showLoveToDeveloper", Boolean.class);
    //</editor-fold>
    //<editor-fold desc="Regions">
    public static final MineMeConfiguration<Boolean> REGIONS_ENABLED = new MineMeConfiguration<>("settings.regions.enabled", Boolean.class);
    public static final MineMeConfiguration<String> REGIONS_PREFIX = new MineMeConfiguration<>("settings.regions.regionPrefix", String.class);
    public static final MineMeConfiguration<String> REGIONS_PROVIDER = new MineMeConfiguration<>("settings.regions.regionProvider", String.class);
    //</editor-fold>
    //<editor-fold desc="Storage">
    public static final MineMeConfiguration<String> STORAGE_TYPE = new MineMeConfiguration<>("settings.storage.type", String.class);
    public static final MineMeConfiguration<Boolean> STORAGE_USE_CREDENTIALS = new MineMeConfiguration<>("settings.storage.settings.credentials.use", Boolean.class);
    public static final MineMeConfiguration<String> STORAGE_USERNAME = new MineMeConfiguration<>("settings.storage.settings.credentials.username", String.class);
    public static final MineMeConfiguration<String> STORAGE_PASSWORD = new MineMeConfiguration<>("settings.storage.settings.credentials.password", String.class);
    public static final MineMeConfiguration<String> STORAGE_HOST = new MineMeConfiguration<>("settings.storage.settings.host", String.class);
    public static final MineMeConfiguration<String> STORAGE_PLAYERS_COLLECTION = new MineMeConfiguration<>("settings.storage.settings.collections.players", String.class);
    public static final MineMeConfiguration<String> STORAGE_MINES_COLLECTION = new MineMeConfiguration<>("settings.storage.settings.collections.mines", String.class);
    public static final MineMeConfiguration<Integer> STORAGE_PORT = new MineMeConfiguration<>("settings.storage.settings.port", Integer.class);
    public static final MineMeConfiguration<String> STORAGE_DATABASE = new MineMeConfiguration<>("settings.storage.settings.database", String.class);
    //</editor-fold>

    public static final MineMeConfiguration<Boolean> HOLOGRAMS_ENABLED = new MineMeConfiguration<>("settings.holograms.enabled", Boolean.class);
    public static final MineMeConfiguration<String> HOLOGRAMS_PROVIDER = new MineMeConfiguration<>("settings.holograms.hologramProvider", String.class);
    public static final MineMeConfiguration<List> HOLOGRAMS_DEFAULT_TEXT = new MineMeConfiguration<>("settings.holograms.defaultHologramText", List.class);
    public static final MineMeConfiguration<Integer> HOLOGRAMS_CUSTOM_REFRESH_RATE = new MineMeConfiguration<>("settings.holograms.customHologramRefreshRate", Integer.class);
    public static final MineMeConfiguration<String> HOLOGRAMS_UPDATE_TYPE = new MineMeConfiguration<>("settings.holograms.hologramUpdateType", String.class);
    public static final MineMeConfiguration<Boolean> HOLOGRAMS_FORCE_DEFAULT_TEXT = new MineMeConfiguration<>("settings.holograms.forceDefaultHologramOnAllMines", Boolean.class);
    //</editor-fold>
    //<editor-fold desc="messages.yml">
    public static final MineMeConfiguration<ConfigurationSection> COLOR_DESIGN = new MineMeConfiguration<>("colors", ConfigurationSection.class);
    public static final MineMeConfiguration<String> PLUGIN_PREFIX = new MineMeConfiguration<>("messages.messagePrefix", String.class);
    public static final MineMeConfiguration<String> MESSAGE_SEPARATOR = new MineMeConfiguration<>("messages.messageSeparator", String.class);
    public static final MineMeConfiguration<String> PLUGIN_HEADER = new MineMeConfiguration<>("messages.header", String.class);
    public static final MineMeConfiguration<String> COLOR_CHAR = new MineMeConfiguration<>("colors.colorChar", String.class);
    public static final MineMeConfiguration<String> DEFAULT_RESET_MESSAGE = new MineMeConfiguration<>("messages.mineReset", String.class);
    //</editor-fold>

    MineMeConfiguration(String path, Class<T> resultType) {
        super(resultType, path);
    }

}
