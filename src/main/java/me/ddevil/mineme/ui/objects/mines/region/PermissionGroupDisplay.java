package me.ddevil.mineme.ui.objects.mines.region;

import me.ddevil.shiroi.ui.menu.InventoryMenu;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.ui.object.InventoryObjectClickListener;
import org.bukkit.inventory.ItemStack;

/**
 * Created by BRUNO II on 12/07/2016.
 */
public class PermissionGroupDisplay extends BasicClickableInventoryObject {

    public PermissionGroupDisplay(ItemStack itemStack, InventoryObjectClickListener interactListener, InventoryMenu menu) {
        super(itemStack, interactListener, menu);
    }
}
