/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.objects.mines.material;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.menus.MainMenu;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.ui.menu.InventoryMenu;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.utils.items.ItemUtils;
import org.bukkit.inventory.ItemStack;

/**
 * @author BRUNO II
 */
public class MineDisplay extends BasicClickableInventoryObject {

    private final Mine mine;
    private final MineMe plugin;

    public MineDisplay(MineMe plugin, Mine m, InventoryMenu menu) {
        super(m.getIcon(), menu);
        this.plugin = plugin;
        this.mine = m;
        this.actionListener = e -> plugin.getGUIManager().getMainMenu().openMineMenu(mine, e.getPlayer());
    }

    public MineDisplay(MineMe plugin, final Mine mine, ItemStack is, MainMenu menu) {
        super(ItemUtils.addToLore(is, new String[]{GUIResources.CLICK_TO_EDIT}), menu);
        this.plugin = plugin;
        this.mine = mine;
        this.actionListener = e -> plugin.getGUIManager().getMainMenu().openMineMenu(mine, e.getPlayer());
    }

    public Mine getMine() {
        return mine;
    }

}
