package me.ddevil.mineme.ui.objects.mines.challenge;

import com.google.common.collect.ImmutableList;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.ChallengeParameters;
import me.ddevil.mineme.messages.MineMeMessageManager;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.ui.menus.MineChallengeMenu;
import me.ddevil.shiroi.events.inventory.InventoryObjectClickEvent;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.ui.object.InventoryObject;
import me.ddevil.shiroi.ui.object.InventoryObjectClickListener;
import me.ddevil.shiroi.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 15/09/2016.
 */
public class ChallengeDisplay extends BasicClickableInventoryObject<MineChallengeMenu, InventoryObject.ItemUpdater> {
    public ChallengeDisplay(MineChallengeMenu menu, ChallengeParameters challengeParameters) {
        super(() -> {
            ImmutableList.Builder<String> lore = new ImmutableList.Builder<String>()
                    .add("$3&lAmplifier: $1&l" + challengeParameters.getChallengeAmplifier())
                    .add("$3&lDuration: $1&l" + challengeParameters.getChallengeDuration());
            Mine<?> mine = menu.getMine();
            if (mine.isRunningAChallenge()) {
                String name = mine.getCurrentChallenge().getName();
                if (name.equalsIgnoreCase(challengeParameters.getChallengeName())) {
                    lore.add("$3&lA challenge of this type is already running!");
                } else {
                    lore.add("$3&lThere is a challenge $1&l" + name + "$3&lrunning in this mine!");
                }
            }
            return new ItemBuilder(Material.BOOK, menu.getPlugin())
                    .setName("$2&l" + challengeParameters.getChallengeName())
                    .setLore(lore.build())
                    .toItemStack();
        }, e -> {
            Mine<?> mine = menu.getMine();
            MineMe plugin = menu.getPlugin();
            MineMeMessageManager mm = plugin.getMessageManager();
            Player player = e.getPlayer();
            if (mine.isRunningAChallenge()) {
                mm.sendMessage(player, "$3There is already a challenge running!");
            } else {
                mine.forceSetCurrentChallenge(plugin.getChallengeManager().requestChallenge(challengeParameters));
            }
        }, menu);
    }
}
