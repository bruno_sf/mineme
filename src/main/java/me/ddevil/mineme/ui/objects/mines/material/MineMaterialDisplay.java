/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.objects.mines.material;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.menus.MineMenu;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.events.inventory.InventoryObjectClickEvent;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.ui.object.InventoryObjectClickListener;
import me.ddevil.shiroi.utils.items.ItemUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * @author BRUNO II
 */
public class MineMaterialDisplay extends BasicClickableInventoryObject {

    private final MineMe plugin;
    protected ItemStack material = null;
    private final Mine mine;

    public ItemStack getMaterial() {
        return material;
    }

    public void setMaterial(ItemStack material) {
        this.material = material;
        update();
    }

    public MineMaterialDisplay(MineMe plugin, Mine m, final MineMenu menu) {
        super(GUIResources.EMPTY_MATERIAL, menu);
        this.plugin = plugin;
        actionListener = new InventoryObjectClickListener() {

            @Override
            public void onInteract(InventoryObjectClickEvent e) {
                Player p = e.getPlayer();
                if (material == null) {
                    ItemStack item = e.getItemInHand();
                    if (item != null) {
                        if (item.getType() != Material.AIR) {
                            item.setAmount(1);
                            mine.addMaterial(item);
                            menu.update();
                        }
                    }
                } else if (e.getClickType() == InventoryObjectClickEvent.InteractionType.INVENTORY_CLICK_LEFT) {
                    menu.openCompositionEditor(material, p);
                } else if (e.getClickType() == InventoryObjectClickEvent.InteractionType.INVENTORY_CLICK_RIGHT) {
                    mine.removeMaterial(material);
                    menu.update();
                }
            }
        };
        this.mine = m;
    }

    @Override
    public void update() {
        if (material != null) {
            icon = generateCompositionItemStack(plugin, mine, material);
        } else {
            icon = GUIResources.EMPTY_MATERIAL;
        }
        icon.setAmount(1);
    }

    public Mine getMine() {
        return mine;
    }

    private static ItemStack generateCompositionItemStack(MineMe plugin, Mine m, ItemStack i) {
        ItemStack is = new ItemStack(i);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(
                plugin.getMessageManager().translateAll("$1" + is.getType() + "$3:$2" + i.getData().getData() + "$3-$1" + m.getComposition().get(i) + "%")
        );
        List<String> lore = ItemUtils.getLore(i);
        lore.add(GUIResources.CLICK_TO_EDIT);
        im.setLore(lore);
        is.setItemMeta(im);
        return is;
    }
}
