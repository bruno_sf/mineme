 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.objects.mines.material;

import java.util.ArrayList;
import java.util.List;

import me.ddevil.mineme.MineMe;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.menus.MineMenu;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.ui.object.internal.BasicInventoryContainer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

/**
 *
 * @author BRUNO II
 */
public class MineMEController extends BasicInventoryContainer<MineMe> {

    protected final Mine mine;
    protected final BasicClickableInventoryObject toogle;
    protected boolean showingMaterial = true;

    protected List<MineMaterialDisplay> materialsDisplays = new ArrayList();
    protected List<MineEffectDisplay> effectsDisplays = new ArrayList();

    public MineMEController(MineMe plugin, MineMenu menu, Mine mine) {
        super(plugin, menu, 36, 44);
        this.toogle = new BasicClickableInventoryObject(GUIResources.TOOGLE_MATERIALS_EFFECTS_SELECTION, e -> toogleDisplay(), menu);
        this.mine = mine;
        for (int i = 0; i < 8; i++) {
            MineMaterialDisplay mmd = new MineMaterialDisplay(plugin,mine, menu);
            materialsDisplays.add(mmd);
            MineEffectDisplay med = new MineEffectDisplay(mine, menu);
            effectsDisplays.add(med);
        }
    }

    public Mine getMine() {
        return mine;
    }

    public void toogleDisplay() {
        showingMaterial = !showingMaterial;
        update();
    }

    public boolean isShowingMaterial() {
        return showingMaterial;
    }

    @Override
    public void update() {
        clear();
        if (showingMaterial) {
            List<ItemStack> materials = mine.getMaterials();
            int i = 0;
            for (MineMaterialDisplay m : materialsDisplays) {
                int index = materialsDisplays.indexOf(m);
                if (materials.size() > index) {
                    m.setMaterial(materials.get(index));
                } else {
                    m.setMaterial(null);
                }
                setObject(i, m);
                i++;
            }
        } else {
            List<PotionEffect> effects = mine.getEffects();
            int i = 0;
            for (MineEffectDisplay m : effectsDisplays) {
                int index = effectsDisplays.indexOf(m);
                if (effects.size() > index) {
                    m.setPotionEffect(effects.get(index));
                } else {
                    m.setPotionEffect(null);
                }
                setObject(i, m);
                i++;
            }
        }
        setObject(8, toogle);
        super.update();
    }

}
