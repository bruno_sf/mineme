/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.objects.mines.material;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.ui.menu.InventoryMenu;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.utils.items.ItemBuilder;
import me.ddevil.shiroi.utils.items.ItemUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author BRUNO II
 */
public class MaterialCompositionChanger extends BasicClickableInventoryObject {

    private final Mine mine;
    private final double percentage;
    private final ItemStack material;
    private final MineMe plugin;

    public MaterialCompositionChanger(MineMe mineMe, final ItemStack material, final InventoryMenu menu, final Mine m, final double percentage) {
        super(generateCompositionChangeItemStack(mineMe, percentage, m, material), e -> {
            m.setMaterialPercentage(material, m.getPercentage(material) + percentage);
            menu.update();
        }, menu);
        this.plugin = mineMe;
        this.material = material;
        this.mine = m;
        this.percentage = percentage;
    }

    public double getPercentage() {
        return percentage;
    }

    @Override
    public void update() {
        icon = generateCompositionChangeItemStack(plugin, percentage, mine, material);
    }

    private static ItemStack generateCompositionChangeItemStack(MineMe plugin, double change, Mine mine, ItemStack material) {
        boolean add = change > 0;
        String prefix = add ? "§a+" : "§c";
        Material m = add ? Material.EMERALD_BLOCK : Material.REDSTONE_BLOCK;
        ItemStack is = new ItemBuilder(m, plugin).setName(prefix + change + "%").toItemStack();
        double finalP = (mine.getPercentage(material) + change);
        if (finalP < 0) {
            finalP = 0;
        }
        ArrayList<String> lore = new ArrayList(Arrays.asList(
                new String[]{
                        "$3Current percentage: $2" + mine.getPercentage(material),
                        "$3Final percentage: $1" + finalP
                }
        )
        );
        lore.add("$3Mine total percentage: $2" + mine.getTotalMaterialPercentage());
        if (mine.isExceedingMaterials()) {
            lore.add("$4Mine is exceeding material by $2" + mine.getExceedingTotal() + "%$4!");
        } else {
            double free = mine.getFreePercentage();
            if (free == 0) {
                lore.add("$1Mine is completely filled!");
            } else {
                lore.add("$3Space left for material:$1 " + free + "%");
            }
        }
        lore.add("§r");
        lore.add("$3Other material:");
        List<ItemStack> materials = mine.getMaterials();
        if (materials.size() - 1 <= 0) {
            lore.add("$4There are no other material!!!");
        } else {
            for (ItemStack s : materials) {
                if (!s.equals(material)) {
                    lore.add(generateItemPrefix(s, mine));
                }
            }
        }
        is = ItemUtils.addToLore(
                is,
                plugin.getMessageManager().translateAll(lore, mine)
        );
        return is;
    }

    private static String generateItemPrefix(ItemStack material, Mine mine) {
        return "$3* $1" + material.getType() + "$3:$2" + material.getData().getData() + "$3-$1" + mine.getPercentage(material) + "%";
    }

    public Mine getMine() {
        return mine;
    }

    public ItemStack getMaterial() {
        return material;
    }

}
