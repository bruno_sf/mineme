/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.objects.mines.material;

import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.menus.MineMenu;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.CustomPlugin;
import me.ddevil.shiroi.events.inventory.InventoryObjectClickEvent;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.ui.object.InventoryObjectClickListener;
import me.ddevil.shiroi.utils.items.ItemBuilder;
import me.ddevil.shiroi.utils.items.ItemUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

/**
 * @author BRUNO II
 */
public class MineEffectDisplay extends BasicClickableInventoryObject {

    protected PotionEffect potionEffect;
    private final Mine mine;

    public void setPotionEffect(PotionEffect potionEffect) {
        this.potionEffect = potionEffect;
        update();
    }

    public PotionEffect getPotionEffect() {
        return potionEffect;
    }

    public MineEffectDisplay(Mine m, final MineMenu menu) {
        super(GUIResources.EMPTY_EFFECT, menu);
        this.mine = m;
        this.actionListener = new InventoryObjectClickListener() {

            @Override
            public void onInteract(InventoryObjectClickEvent e) {
                if (potionEffect == null) {
                    ItemStack itemInHand = e.getItemInHand();
                    if (itemInHand != null) {
                        if (itemInHand.hasItemMeta()) {
                            ItemMeta itemMeta = itemInHand.getItemMeta();
                            if (itemMeta instanceof PotionMeta) {
                                PotionMeta m = (PotionMeta) itemMeta;
                                PotionType type = m.getBasePotionData().getType();
                                potionEffect = type.getEffectType().createEffect(21, type.getMaxLevel());
                                mine.addPotionEffect(potionEffect);
                                menu.update();
                            }
                        }
                    }
                } else if (e.getClickType() == InventoryObjectClickEvent.InteractionType.INVENTORY_CLICK_LEFT) {
                    PotionEffectType type = potionEffect.getType();
                    int amplifier = potionEffect.getAmplifier();
                    PotionEffect newEffect;
                    if (amplifier >= 10) {
                        newEffect = new PotionEffect(type, potionEffect.getDuration(), 0);
                    } else {
                        newEffect = new PotionEffect(type, potionEffect.getDuration(), amplifier + 1);
                    }
                    potionEffect = newEffect;
                    mine.addPotionEffect(potionEffect);
                    menu.update();
                } else if (e.getClickType() == InventoryObjectClickEvent.InteractionType.INVENTORY_CLICK_RIGHT) {
                    mine.removePotionEffect(potionEffect.getType());
                    potionEffect = null;
                    menu.update();
                }
            }
        };
    }

    public Mine getMine() {
        return mine;
    }

    private static ItemStack generateIcon(PotionEffect e, CustomPlugin plugin) {
        ItemStack createItem = new ItemBuilder(Material.POTION, plugin).setName("§e" + e.getType().getName()).toItemStack();
        createItem = ItemUtils.addToLore(createItem, "§7Amplifier: §a" + e.getAmplifier());
        return createItem;
    }

    @Override
    public void update() {
        if (potionEffect != null) {
            icon = generateIcon(potionEffect, menu.getPlugin());
        } else {
            icon = GUIResources.EMPTY_EFFECT;
        }
    }
}
