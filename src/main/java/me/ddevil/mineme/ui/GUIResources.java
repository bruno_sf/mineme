/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.ui;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.utils.items.ItemBuilder;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

/**
 * @author DDevil_
 */
public class GUIResources {

    private static FileConfiguration guiConfig;
    private static ConfigurationSection ITEMS_SECTION;
    private static ConfigurationSection STRINGS_SECTION;
    //Placeholders
    //Mine utils


    public static final int INVENTORY_SIZE = 6;
    private static MineMe plugin;
    public static ItemStack SPLITTER;
    public static ItemStack EMPTY_MATERIAL;
    public static ItemStack EMPTY_EFFECT;
    public static ItemStack EMPTY_NEUTRAL;
    public static ItemStack BACK_BUTTON;
    public static ItemStack TELEPORT_TO_MINE_BUTTON;
    public static ItemStack TOOGLE_MATERIALS_EFFECTS_SELECTION;
    public static ItemStack FILL_MINE;
    public static ItemStack BALANCE_MATERIALS;
    public static ItemStack RESET_MATERIALS;
    public static ItemStack REMOVE_MATERIAL_BUTTON;
    public static ItemStack RESET_MINE_BUTTON;
    public static ItemStack CLEAR_MINE;
    public static ItemStack DELETE_MINE_BUTTON;
    public static ItemStack MINE_CLEAR_MATERIALS;
    public static ItemStack DISABLE_MINE_BUTTON;
    public static ItemStack NOT_LOADED_MINES;
    public static ItemStack NO_MINE_TO_DISPLAY;
    public static ItemStack NOT_LOADED_ICON;
    public static ItemStack MINE_COMPOSITION_INFORMATION;
    public static ItemStack REFRESH_MENU;
    public static ItemStack FILES_SEARCH_RESULT;
    public static ItemStack REGION_BUTTON;
    public static ItemStack RECALCULATOR;
    public static ItemStack RECALCULATING;
    public static ItemStack CHALLENGES_BUTTON;

    public static String FOUND_MINE_FILES;
    public static String NO_MISFORMATTED_FILES;
    public static String CLICK_TO_EDIT;
    public static String CLICK_TO_SEE;
    public static String CLICK_TO_LOAD;
    public static String CLICK_TO_REMOVE;

    public static ItemStack generateNotYetLoadedIcon(Mine m) {
        ItemStack itemStack = new ItemStack(NOT_LOADED_ICON);
        return plugin.getMessageManager().translateItemStack(itemStack, m);
    }


    public static ItemStack generateInformationItem(Mine mine) {
        return plugin.getMessageManager().translateItemStack(new ItemStack(MINE_COMPOSITION_INFORMATION),
                mine);
    }

    public static String loadStringFromConfig(String configSection) {
        return plugin.getMessageManager().translateAll(STRINGS_SECTION.getString(configSection));
    }

    public static ItemStack loadFromConfig(String configSection) {
        return ItemBuilder.createItem(ITEMS_SECTION.getConfigurationSection(configSection), plugin).toItemStack();
    }

    public static void setup(MineMe up) {
        GUIResources.plugin = up;
        guiConfig = plugin.getConfigurationManager().getGUIConfig();
        ITEMS_SECTION = guiConfig.getConfigurationSection("globalItems");
        STRINGS_SECTION = guiConfig.getConfigurationSection("me.ddevil.config");
        SPLITTER = loadFromConfig("splitter");
        EMPTY_MATERIAL = loadFromConfig("emptyMaterial");
        EMPTY_EFFECT = loadFromConfig("emptyEffect");
        EMPTY_NEUTRAL = loadFromConfig("empty");
        //Iteraction
        BACK_BUTTON = loadFromConfig("back");
        TELEPORT_TO_MINE_BUTTON = loadFromConfig("teleporter");
        TOOGLE_MATERIALS_EFFECTS_SELECTION = loadFromConfig("toogleMaterialEditor");
        FILL_MINE = loadFromConfig("fillMine");
        BALANCE_MATERIALS = loadFromConfig("balanceMaterials");
        RESET_MATERIALS = loadFromConfig("resetMaterials");
        //Removers
        REMOVE_MATERIAL_BUTTON = loadFromConfig("removeMaterial");
        RESET_MINE_BUTTON = loadFromConfig("resetNow");
        CLEAR_MINE = loadFromConfig("clearMine");
        DELETE_MINE_BUTTON = loadFromConfig("deleteMine");
        MINE_CLEAR_MATERIALS = loadFromConfig("clearMaterials");
        DISABLE_MINE_BUTTON = loadFromConfig("disableMine");
        //Loading
        NOT_LOADED_MINES = loadFromConfig("unloadedMines");
        NO_MINE_TO_DISPLAY = loadFromConfig("noMine");
        NOT_LOADED_ICON = loadFromConfig("iconNotLoaded");
        REGION_BUTTON = loadFromConfig("regionButton");
        //Stats
        MINE_COMPOSITION_INFORMATION = loadFromConfig("info");
        REFRESH_MENU = loadFromConfig("refresh");
        FILES_SEARCH_RESULT = loadFromConfig("couldNotLoadFiles");

        FOUND_MINE_FILES = loadStringFromConfig("foundFiles");
        NO_MISFORMATTED_FILES = loadStringFromConfig("noMisformattedFiles");
        CLICK_TO_EDIT = loadStringFromConfig("clickToEdit");
        CLICK_TO_SEE = loadStringFromConfig("clickToSeeMine");
        CLICK_TO_LOAD = loadStringFromConfig("clickToLoad");
        CLICK_TO_REMOVE = loadStringFromConfig("clickToRemove");

        RECALCULATOR = loadFromConfig("recalculator");
        RECALCULATING = loadFromConfig("recalculatingStats");
        CHALLENGES_BUTTON =loadFromConfig("challengeButton");
    }
}
