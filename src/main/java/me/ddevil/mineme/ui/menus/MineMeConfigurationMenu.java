/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.menus;

import me.ddevil.shiroi.ui.menu.BasicInventoryMenu;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.objects.mines.material.MineMeConfigurationToogler;

/**
 *
 * @author BRUNO II
 */
public class MineMeConfigurationMenu extends BasicInventoryMenu {

    public MineMeConfigurationMenu(MineMe plugin, String name) {
        super(plugin, name, GUIResources.INVENTORY_SIZE);
        holograms = new MineMeConfigurationToogler(null, this, null, name);
    }
    private final BasicClickableInventoryObject holograms;

    @Override
    protected void setupItems() {
        registerInventoryObject(holograms, 0);
    }

    @Override
    public void update() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
