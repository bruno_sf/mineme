/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.menus;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.objects.mines.material.MaterialCompositionChanger;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.ui.menu.BasicInventoryMenu;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.ui.object.internal.BackButton;
import me.ddevil.shiroi.ui.object.internal.BasicInventoryContainer;
import me.ddevil.shiroi.utils.items.ItemBuilder;
import me.ddevil.shiroi.utils.items.ItemUtils;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HP
 */
public class MineCompositionEditorMenu extends BasicInventoryMenu<MineMe, MineCompositionEditorMenu> {

    private final ItemStack material;
    private final Mine mine;
    private final MineMenu mineMenu;

    private static String getName(MineMe plugin, ItemStack material) {
        return plugin.getMessageManager().translateAll("$2" + material.getType() + "$3:$1" + material.getData().getData());
    }

    public MineCompositionEditorMenu(MineMe plugin, final Mine owner, final ItemStack material, MineMenu menu) {
        super(plugin, getName(plugin, material), GUIResources.INVENTORY_SIZE);
        this.mine = owner;
        this.mineMenu = menu;
        this.material = owner.getRelativeItemStackInComposition(material);
        this.backButton = new BackButton(mineMenu, GUIResources.BACK_BUTTON, this);
        this.removeButton = new BasicClickableInventoryObject(GUIResources.REMOVE_MATERIAL_BUTTON, e -> {
            mine.removeMaterial(material);
            mineMenu.open(e.getPlayer());
        }, this);
        this.changers = new BasicInventoryContainer(plugin, this, 9, 25);
        this.options = new BasicInventoryContainer(plugin, this, 36, 43);
    }

    private final BackButton backButton;
    private final BasicInventoryContainer options;
    private final BasicClickableInventoryObject removeButton;
    private final double[] percentages = {50, 25, 10, 5, 2, 1, 0.5, 0.1};
    private final BasicInventoryContainer changers;

    @Override
    protected void setupItems() {
        clearAndFill(GUIResources.SPLITTER);
        registerInventoryObject(changers, 9);
        registerInventoryObject(options, 36);
        registerInventoryObject(backButton, 45);
        options.clearAndFill(GUIResources.EMPTY_NEUTRAL);
        options.addObject(removeButton);
        int slot = 0;
        for (double d : percentages) {
            MaterialCompositionChanger pos = new MaterialCompositionChanger(plugin, material, this, mine, d);
            MaterialCompositionChanger neg = new MaterialCompositionChanger(plugin, material, this, mine, d * -1);
            changers.setObject(slot, pos);
            changers.setObject(slot + 8, neg);
            slot++;
        }
    }

    @Override
    public void update() {
        changers.update();
        options.setItem(7, generateItemStat());
    }

    private ItemStack generateItemStat() {
        ItemStack is = new ItemBuilder(material, plugin).setName(plugin.getMessageManager().translateAll(generateItemPrefix())).toItemStack();
        ArrayList<String> lore = new ArrayList();
        lore.add("$3Mine total percentage: $2" + mine.getTotalMaterialPercentage());
        if (mine.isExceedingMaterials()) {
            lore.add("$4Mine is exceeding material by $2" + mine.getExceedingTotal() + "%$4!");
        } else {
            double free = mine.getFreePercentage();
            if (free == 0) {
                lore.add("$1Mine is completely filled!");
            } else {
                lore.add("$3Space left for material:$1 " + free + "%");
            }
        }
        lore.add("§r");
        lore.add("$3Other material:");
        List<ItemStack> materials = mine.getMaterials();
        if (materials.size() - 1 <= 0) {
            lore.add("$4There are no other material!!!");
        } else {
            for (ItemStack s : materials) {
                if (!s.equals(material)) {
                    lore.add(generateItemPrefix(s));
                }
            }
        }
        is = ItemUtils.addToLore(
                is,
                plugin.getMessageManager().translateAll(lore, mine)
        );
        return is;
    }

    private String generateItemPrefix() {
        return "$3* $1" + material.getType() + "$3:$2" + material.getData().getData() + "$3-$1" + mine.getPercentage(material) + "%";
    }

    private String generateItemPrefix(ItemStack material) {
        return "$3* $1" + material.getType() + "$3:$2" + material.getData().getData() + "$3-$1" + mine.getPercentage(material) + "%";
    }
}
