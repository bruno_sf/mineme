/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.menus;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineConfig;
import me.ddevil.mineme.task.MineFilesFinder;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.objects.mines.material.MineDisplay;
import me.ddevil.shiroi.ui.menu.BasicInventoryMenu;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.ui.object.BasicInventoryItem;
import me.ddevil.shiroi.ui.object.InventoryObject;
import me.ddevil.shiroi.ui.object.internal.ScrollableInventoryContainer;
import me.ddevil.shiroi.utils.item.ItemUtils;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

/**
 * @author DDevil_
 */
public class MainMenu extends BasicInventoryMenu<MineMe> {


    public MainMenu(MineMe plugin, String name) {
        super(plugin, name, GUIResources.INVENTORY_SIZE);
        this.mineList = new ScrollableInventoryContainer(plugin, this, 0, 33);
        this.unloadedMineList = new ScrollableInventoryContainer(plugin, this, 36, 52);
        this.refreshButton = new BasicClickableInventoryObject(GUIResources.REFRESH_MENU, e -> update(), this);
    }

    protected ItemStack mainInventoryIcon;
    private final BasicClickableInventoryObject refreshButton;
    private final ScrollableInventoryContainer<MineMe, MineDisplay> mineList;
    private final ScrollableInventoryContainer unloadedMineList;

    @Override
    public void update() {
        //Already loaded mines
        mineList.clear();
        for (Mine mine : plugin.getMineManager().getMines()) {
            try {
                ItemStack is = new ItemStack(mine.getIcon());
                mineList.addObject(new MineDisplay(plugin, mine, is, this));
            } catch (Exception e) {
                plugin.printException("There was a problem while loading mine " + mine.getName() + "'s icon", e);
            }
        }
        mineList.update();
        unloadedMineList.clear();
        final MineFilesFinder mineFilesFinder = new MineFilesFinder((MineMe) plugin);
        objects.remove(44);
        mineFilesFinder.addListener(() -> {
            registerInventoryObject(new BasicInventoryItem(MainMenu.this, mineFilesFinder.getNotLoadedItemStat()), 44);
            List<MineConfig> mines = mineFilesFinder.getMines();
            plugin.debug("Found " + mines.size() + " files in " + mineFilesFinder.getTotalTimeSeconds() + " seconds (" + mineFilesFinder.getTotalTime() + "ms)");
            for (final MineConfig mine : mines) {
                ItemStack icon = generateItemStack(mine);
                icon = ItemUtils.addToLore(icon, new String[]{GUIResources.CLICK_TO_LOAD});
                unloadedMineList.addObject(new BasicClickableInventoryObject(icon, e -> {
                    Mine loadMine = plugin.getMineManager().loadMine(mine);
                    loadMine.setEnabled(true);
                    loadMine.save();
                    Player player = e.getPlayer();
                    plugin.getMessageManager().sendMessage(player, "Mine " + loadMine.getName() + " was loaded!");
                    update();
                }, MainMenu.this));
            }
            unloadedMineList.update();
        });
        mineFilesFinder.start();
    }

    @Override
    protected void setupItems() {
        this.mainInventoryIcon = plugin.getMessageManager().createIcon(plugin.getConfigurationManager().getGUIConfig().getConfigurationSection("mainMenu.icons.main"));
        mineList.setBackground(GUIResources.NO_MINE_TO_DISPLAY);
        unloadedMineList.setBackground(GUIResources.NOT_LOADED_MINES);
        for (int i : InventoryUtils.getLane(mainInventory, InventoryUtils.getTotalLanes(mainInventory) - 1)) {
            setItem(GUIResources.SPLITTER, i - 18);
            setItem(GUIResources.SPLITTER, i);
        }
        setItem(mainInventoryIcon, InventoryUtils.getBottomMiddlePoint(mainInventory));
        setItem(ItemUtils.NA, 8);
        setItem(ItemUtils.NA, 17);
        setItem(GUIResources.SPLITTER, 7);
        setItem(GUIResources.SPLITTER, 16);
        setItem(GUIResources.SPLITTER, 25);
        registerInventoryObject(refreshButton, );
        registerInventoryObject(mineList, 0);
        registerInventoryObject(unloadedMineList, 36);
    }

    private ItemStack generateItemStack(MineConfig config) {
        FileConfiguration xconfig = config.getConfig();
        ItemStack createItem = new ItemBuilder(new ItemStack(
                Material.valueOf(xconfig.getString("icon.type")),
                1,
                ((Integer) xconfig.getInt("icon.data")).shortValue()
        ), plugin).setName("§a" + config.getName() + ".yml").
                toItemStack();
        createItem = ItemUtils.addToLore(createItem, new String[]{
                "§7Alias: " + plugin.getMessageManager().translateAll(config.getAlias()),
                "§7Type: §d" + config.getType(),
                "§7World: §e" + config.getWorld().getName()
        });
        return createItem;
    }

    protected final HashMap<Mine, MineMenu> inventories = new HashMap();

    public void openMineMenu(Mine m, Player p) {
        getMineInventory(m).open(p);
    }

    public MineMenu getMineInventory(Mine m) {
        if (inventories.containsKey(m)) {
            return inventories.get(m);
        } else {
            plugin.debug("Creating new MineMenu for mine " + m.getName(), DebugLevel.NO_BIG_DEAL);
            MineMenu inv = new MineMenu(plugin, m).setup();
            inventories.put(m, inv);
            return inv;
        }
    }
}
