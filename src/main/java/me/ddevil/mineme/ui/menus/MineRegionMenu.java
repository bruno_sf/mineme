package me.ddevil.mineme.ui.menus;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.regions.MineRegion;
import me.ddevil.mineme.regions.MineRegionManager;
import me.ddevil.shiroi.ui.menu.BasicInventoryMenu;
import me.ddevil.shiroi.ui.object.internal.BackButton;
import me.ddevil.shiroi.utils.inventory.InventoryUtils;

/**
 * Created by BRUNO II on 12/07/2016.
 */
public class MineRegionMenu extends BasicInventoryMenu<MineMe, MineRegionMenu> {
    private final MineRegionManager regionManager;
    private final MineRegion region;
    private final BackButton backButton;

    public MineRegionMenu(MineMe plugin, MineMenu inv) {
        super(plugin, inv.getBukkitInventory().getTitle(), GUIResources.INVENTORY_SIZE);
        this.regionManager = plugin.getRegionManager();
        Mine m = inv.getMine();
        region = m.getRegion();
        this.backButton = new BackButton(inv, GUIResources.BACK_BUTTON, this);
    }
    @Override
    protected void setupItems() {
        for (int i : InventoryUtils.getLane(mainInventory, InventoryUtils.getTotalLanes(mainInventory) - 1)) {
            setItem(GUIResources.SPLITTER, i);
        }
        for (int i : InventoryUtils.getColumn(mainInventory, 4)) {
            setItem(GUIResources.SPLITTER, i);
        }
        registerInventoryObject(backButton, 45);
    }

    @Override
    public void update() {

    }
}
