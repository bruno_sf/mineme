package me.ddevil.mineme.ui.menus;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.ui.objects.mines.challenge.ChallengeDisplay;
import me.ddevil.shiroi.ui.menu.BasicInventoryMenu;
import me.ddevil.shiroi.ui.object.internal.ScrollableInventoryContainer;

/**
 * Created by bruno on 15/09/2016.
 */
public class MineChallengeMenu extends BasicInventoryMenu<MineMe, MineChallengeMenu> {
    private final Mine<?> mine;
    private final ScrollableInventoryContainer<MineMe, ChallengeDisplay> challengeList;

    public MineChallengeMenu(MineMe plugin, Mine<?> mine) {
        super(plugin, plugin.getMessageManager().translateAll("$2&lChallenges"), 3);
        this.mine = mine;
        this.challengeList = new ScrollableInventoryContainer<>(plugin, this, 0, 26);
    }

    @Override
    public void update() {
        challengeList.update();
    }

    @Override
    protected void setupItems() {
        registerInventoryObject(challengeList, 0);
        mine.getChallengeParameters().forEach(challengeParameters -> challengeList.addObject(
                new ChallengeDisplay(this, challengeParameters))
        );
    }

    public Mine<?> getMine() {
        return mine;
    }
}
