/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.mineme.ui.menus;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.messages.MineMeMessageManager;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineUtils;
import me.ddevil.mineme.task.MineStatsRecalculator;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.mineme.ui.objects.mines.material.MineMEController;
import me.ddevil.shiroi.events.inventory.InventoryObjectClickEvent;
import me.ddevil.shiroi.ui.menu.BasicInventoryMenu;
import me.ddevil.shiroi.ui.object.BasicClickableInventoryObject;
import me.ddevil.shiroi.ui.object.InventoryObject;
import me.ddevil.shiroi.ui.object.InventoryObjectClickListener;
import me.ddevil.shiroi.ui.object.internal.BackButton;
import me.ddevil.shiroi.utils.inventory.InventoryUtils;
import me.ddevil.shiroi.utils.items.ItemUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author HP
 */
public class MineMenu extends BasicInventoryMenu<MineMe, MineMenu> {

    private final HashMap<ItemStack, MineCompositionEditorMenu> compositionEditInventories = new HashMap();
    private final Mine mine;
    private final boolean useRegions;

    private boolean recalculatingStats = false;
    private final MineChallengeMenu challengeMenu;
    private final MineRegionMenu regionMenu;
    private final BackButton backButton;
    private final BasicClickableInventoryObject regionButton;
    private final BasicClickableInventoryObject recalculateStats;
    private final BasicClickableInventoryObject challengeButton;
    private final BasicClickableInventoryObject resetMaterials;
    private final BasicClickableInventoryObject balanceMaterials;
    private final BasicClickableInventoryObject clearMine;
    private final BasicClickableInventoryObject teleporter;
    private final BasicClickableInventoryObject reseter;
    private final BasicClickableInventoryObject materialClearer;
    private final BasicClickableInventoryObject disableMineButton;
    private final BasicClickableInventoryObject deleteMineButton;
    private final BasicClickableInventoryObject fillMine;
    private final MineMEController mineMEController;

    public MineMenu(MineMe plugin, Mine m) {
        super(plugin, m.getAlias(), GUIResources.INVENTORY_SIZE);
        this.mine = m;
        this.backButton = new BackButton(plugin.getGUIManager().getMainMenu(), GUIResources.BACK_BUTTON, this);
        this.mineMEController = new MineMEController(plugin, this, mine);
        this.teleporter = new BasicClickableInventoryObject<>(GUIResources.TELEPORT_TO_MINE_BUTTON, e -> e.getPlayer().teleport(mine.getTopCenterLocation()), this);
        this.reseter = new BasicClickableInventoryObject<>(GUIResources.RESET_MINE_BUTTON, e -> {
            mine.reset();
            update();
        }, this);
        materialClearer = new BasicClickableInventoryObject<>(GUIResources.MINE_CLEAR_MATERIALS, e -> {
            mine.clearMaterials();
            update();
        }, this);
        MineMeMessageManager messageManager = plugin.getMessageManager();
        deleteMineButton = new BasicClickableInventoryObject<>(GUIResources.DELETE_MINE_BUTTON, e -> {
            Player player = e.getPlayer();
            player.closeInventory();
            messageManager.sendMessage(player, "Mine " + mine.getName() + " was deleted!");
            mine.delete();
        }, this);
        disableMineButton = new BasicClickableInventoryObject<>(GUIResources.DISABLE_MINE_BUTTON, e -> {
            Player player = e.getPlayer();
            messageManager.sendMessage(player, "Mine " + mine.getName() + " was disabled!");
            mine.disable();
            player.closeInventory();
        }, this);
        clearMine = new BasicClickableInventoryObject<>(GUIResources.CLEAR_MINE, e -> mine.clear(), this);
        this.fillMine = new BasicClickableInventoryObject<>(GUIResources.FILL_MINE, e -> {
            ItemStack item = e.getItemInHand();
            if (item != null) {
                if (item.getType() != Material.AIR) {
                    mine.fill(item);
                    messageManager.sendMessage(e.getPlayer(), mine.getName() + " was filled with $2" + ItemUtils.toString(item));
                }
            }
        }, this);
        this.balanceMaterials = new BasicClickableInventoryObject<>(GUIResources.BALANCE_MATERIALS, e -> {
            List<ItemStack> materials = mine.getMaterials();
            if (materials.isEmpty()) {
                messageManager.sendMessage(e.getPlayer(), "There are no material to balance :(");
                return;
            }
            double percentage = 100 / materials.size();
            for (ItemStack material : materials) {
                mine.setMaterialPercentage(material, percentage);
            }
            messageManager.sendMessage(e.getPlayer(), "Materials were balanced with $2" + percentage + " $3percent each");
            update();
        }, this);
        this.resetMaterials = new BasicClickableInventoryObject<>(GUIResources.RESET_MATERIALS, e -> {
            List<ItemStack> materials = mine.getMaterials();

            for (ItemStack material : materials) {
                mine.setMaterialPercentage(material, 5);
            }
            messageManager.sendMessage(e.getPlayer(), "material were reseted to $25% $3each");
            update();
        }, this);
        this.recalculateStats = new BasicClickableInventoryObject<>(() -> {
            if (recalculatingStats) {
                return GUIResources.RECALCULATING;
            } else {
                return GUIResources.RECALCULATOR;
            }
        }, e -> {
            MineStatsRecalculator mineStatsRecalculator = mine.recalculateStatistics();
            mineStatsRecalculator.addListener(() -> {
                messageManager.sendMessage(e.getPlayer(), "Recalculated! Total broken blocks = $1" + mineStatsRecalculator.getMineInfo().getTotalBrokenBlocks());
                recalculatingStats = false;
                update();
            });
            mineStatsRecalculator.start();
            recalculatingStats = true;
            update();
        }, this);
        this.useRegions = plugin.getConfigurationManager().useRegions();
        if (useRegions) {
            regionMenu = new MineRegionMenu(plugin, this);
            regionButton = new BasicClickableInventoryObject(GUIResources.REGION_BUTTON, inventoryObjectClickEvent -> regionMenu.open(inventoryObjectClickEvent.getPlayer()), this);
        } else {
            regionButton = null;
            regionMenu = null;
        }
        if (mine.useChallenges()) {
            challengeMenu = new MineChallengeMenu(plugin, mine);
            challengeButton = new BasicClickableInventoryObject<>(GUIResources.CHALLENGES_BUTTON, e -> challengeMenu.open(e.getPlayer()), this);
        } else {
            challengeMenu = null;
            challengeButton = null;
        }
    }


    @Override
    protected void setupItems() {
        for (int i : InventoryUtils.getLane(mainInventory, InventoryUtils.getTotalLanes(mainInventory) - 1)) {
            setItem(GUIResources.SPLITTER, i);
            setItem(GUIResources.SPLITTER, i - 18);
        }
        for (int i : new int[]{6, 15, 24}) {
            setItem(GUIResources.SPLITTER, i);
        }
        if (useRegions) {
            registerInventoryObject(regionButton, 8);
        }
        if (mine.useChallenges()) {
            registerInventoryObject(challengeButton, 7);
        }
        registerInventoryObject(backButton, 45);
        registerInventoryObject(teleporter, 0);
        registerInventoryObject(reseter, 1);
        registerInventoryObject(clearMine, 2);
        registerInventoryObject(fillMine, 3);
        registerInventoryObject(materialClearer, 9);
        registerInventoryObject(balanceMaterials, 10);
        registerInventoryObject(resetMaterials, 11);
        registerInventoryObject(recalculateStats, 12);
        registerInventoryObject(disableMineButton, 22);
        registerInventoryObject(deleteMineButton, 23);
        registerInventoryObject(mineMEController, InventoryUtils.getFirstSlotInLane(4));
        setItem(GUIResources.generateInformationItem(mine), InventoryUtils.getBottomMiddlePoint(mainInventory) + 4);
        setItem(mine.getIcon(), InventoryUtils.getBottomMiddlePoint(mainInventory));
    }

    @Override
    public void update() {
        Map<ItemStack, Double> composition = mine.getComposition();
        Stream<ItemStack> stream = mine.getMaterials().stream();
        stream.filter(material -> composition.get(material) == null).forEach(material -> composition.put(material, 0d));
        setItem(GUIResources.generateInformationItem(mine), InventoryUtils.getBottomMiddlePoint(mainInventory) + 4);
        mineMEController.update();
        recalculateStats.update();
    }

    public Mine getMine() {
        return mine;
    }

    public void openCompositionEditor(ItemStack itemStackInComposition, Player p) {
        getMineCompositionEditorMenu(itemStackInComposition).open(p);
    }

    private MineCompositionEditorMenu getMineCompositionEditorMenu(ItemStack item) {
        item = MineUtils.getItemStackInComposition(mine, item);
        MineCompositionEditorMenu menu;
        if (compositionEditInventories.containsKey(item)) {
            menu = compositionEditInventories.get(item);
        } else {
            menu = new MineCompositionEditorMenu((MineMe) plugin, mine, item, this);
        }
        compositionEditInventories.put(item, menu);
        return menu;
    }

}
