/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.ui;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.ui.menus.MainMenu;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.misc.BaseManager;
import me.ddevil.shiroi.utils.DebugLevel;
import me.ddevil.shiroi.utils.items.ItemBuilder;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

/**
 * @author DDevil_
 */
public class GUIManager extends BaseManager<MineMe, GUIManager> {

    public boolean ready = false;
    private MainMenu mainMenu;

    public GUIManager(MineMe plugin) {
        super(plugin);
    }

    public GUIManager setup() {
        ConfigurationSection mmConfig = plugin.getConfigurationManager().getGUIConfig().getConfigurationSection("mainMenu");
        String mainMenuName = plugin.getMessageManager().translateAll(mmConfig.getString("name"));
        plugin.debug();
        plugin.debug("Loading MEGUI's mainMenu title " + mainMenuName, DebugLevel.NO_BIG_DEAL);
        plugin.debug();
        if (mainMenu != null) {
            mainMenu.disable();
        }
        mainMenu = new MainMenu(plugin, mainMenuName).setup();
        ready = true;
        return this;
    }

    private ItemStack loadFromConfig(ConfigurationSection configSection) {
        return ItemBuilder.createItem(configSection, plugin).toItemStack();
    }

    public ItemStack getMineIcon(Mine m) {
        return null;
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }
}
