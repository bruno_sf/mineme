package me.ddevil.mineme.events.players;

import me.ddevil.shiroi.events.CancellableEvent;
import me.ddevil.shiroi.events.CancellableWrapperEvent;

/**
 * Created by BRUNO II on 10/07/2016.
 */
public class PlayerPickupPossibleConflictItemEvent<E extends CancellableEvent> extends CancellableWrapperEvent<PlayerPickupPossibleConflictItemEvent, E>{
    public PlayerPickupPossibleConflictItemEvent(E event) {
        super(event);
    }
}
