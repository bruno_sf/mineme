package me.ddevil.mineme.events.players;

import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.events.CancellableEvent;
import me.ddevil.shiroi.events.CustomEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;

/**
 * Created by BRUNO II on 06/07/2016.
 */
public class PlayerLeaveMineEvent extends CancellableEvent<PlayerLeaveMineEvent> {
    private final Player player;
    private final Mine mine;

    public PlayerLeaveMineEvent(Player player, Mine mine) {
        this.player = player;
        this.mine = mine;
    }
}
