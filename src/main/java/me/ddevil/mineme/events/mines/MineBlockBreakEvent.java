package me.ddevil.mineme.events.mines;

import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.events.CancellableEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by BRUNO II on 13/07/2016.
 */
public class MineBlockBreakEvent extends CancellableMineEvent<MineBlockBreakEvent> {
    private final Block block;
    private final Player player;
    private final Cancellable event;

    public MineBlockBreakEvent(Mine mine, Block block, Player player, Cancellable e) {
        super(mine);
        this.block = block;
        this.player = player;
        this.event = e;
    }

    public Block getBlock() {
        return block;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public void setCancelled(boolean cancel) {
        super.setCancelled(cancel);
        event.setCancelled(cancel);
    }
}
