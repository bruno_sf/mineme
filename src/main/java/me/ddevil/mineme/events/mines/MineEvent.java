package me.ddevil.mineme.events.mines;

import me.ddevil.mineme.mines.Mine;
import me.ddevil.shiroi.events.CustomEvent;

/**
 * Created by BRUNO II on 10/08/2016.
 */
public class MineEvent<E extends CustomEvent> extends CustomEvent<E> {
    protected final Mine mine;

    public MineEvent(Mine mine) {
        this.mine = mine;
    }

    public Mine getMine() {
        return mine;
    }
}
