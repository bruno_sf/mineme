package me.ddevil.mineme.events.mines;

import me.ddevil.mineme.mines.Mine;
import org.bukkit.event.Cancellable;

/**
 * Created by BRUNO II on 10/08/2016.
 */
public class CancellableMineEvent<E extends CancellableMineEvent> extends MineEvent<E> implements Cancellable {

    protected boolean cancelled;

    public CancellableMineEvent(Mine mine) {
        super(mine);
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}
