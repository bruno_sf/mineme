package me.ddevil.mineme.events.mines.challenges;

import me.ddevil.mineme.challenges.Challenge;
import me.ddevil.shiroi.events.CancellableEvent;

/**
 * Created by BRUNO II on 12/07/2016.
 */
public class ChallengeEndEvent extends CancellableEvent<ChallengeEndEvent> {
    public ChallengeEndEvent(Challenge challenge) {
        this.challenge = challenge;
    }

    public Challenge getChallenge() {
        return challenge;
    }

    private final Challenge challenge;
}
