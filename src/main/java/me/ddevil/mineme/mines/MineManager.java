/* 
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.mines;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.event.extent.EditSessionEvent;
import com.sk89q.worldedit.extension.platform.Actor;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.extent.logging.AbstractLoggingExtent;
import com.sk89q.worldedit.util.eventbus.Subscribe;
import com.sk89q.worldedit.world.World;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.configuration.MineMeConfiguration;
import me.ddevil.mineme.events.mines.MineLoadEvent;
import me.ddevil.mineme.messages.MineMeMessageManager;
import me.ddevil.mineme.placeholders.PlaceholderManager;
import me.ddevil.shiroi.config.ConstantsConfigurationManager;
import me.ddevil.shiroi.misc.BukkitToggleable;
import me.ddevil.shiroi.misc.DebugLevel;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DDevil_
 */
public class MineManager extends BukkitToggleable<MineMe> {

    private final ArrayList<Mine> MINES = new ArrayList();
    private final ConstantsConfigurationManager<?> config;

    public MineManager(MineMe plugin) {
        super(plugin);
        this.config = plugin.getConfigManager();
    }

    public List<Mine> getMines() {
        return MINES;
    }

    public boolean isNameAvailable(String name) {
        for (Mine mine : MINES) {
            if (mine.getName().equalsIgnoreCase(name)) {
                return false;
            }
        }
        return true;
    }

    public Mine loadMine(MineConfig c) {
        Mine m = MineConfig.loadMine(plugin, c);
        try {
            if (config.getValue(MineMeConfiguration.HOLOGRAMS_ENABLED)) {
                if (m instanceof HologramCompatibleMine) {
                    plugin.debug("Mine " + m.getName() + " is holograms compatible! Creating holograms...", DebugLevel.NO_BIG_DEAL);
                    HologramCompatibleMine h = (HologramCompatibleMine) m;
                    h.setupHolograms();
                }
            }
        } finally {
            m.reset();
        }
        registerMine(m);
        new MineLoadEvent(m).call();
        return m;
    }

    public void registerMine(Mine m) {
        MINES.add(m);
        PlaceholderManager placeholderManager = plugin.getPlaceholderManager();
        plugin.registerListener(m);
        for (PlaceholderManager.PlaceholderProvider placeholderProvider : PlaceholderManager.PlaceholderProvider.values()) {
            if (placeholderManager.isUsable(placeholderProvider)) {
                if (!placeholderManager.isPlaceholderRegistered(m, placeholderProvider)) {
                    placeholderManager.registerMinePlaceholders(m, placeholderProvider);
                }
            }
        }
        plugin.debug("Mine " + m.getName() + " registered to manager!");
    }

    public void unregisterMines() {
        MINES.clear();
        MINES.forEach(this::unregisterMine);
        plugin.debug("Unloaded all mines!");
    }

    public void unregisterMine(Mine mine) {
        MINES.remove(mine);
        plugin.unregisterListener(mine);
        plugin.debug("Unloaded mine " + mine.getName() + "!");
    }

    public Mine getMine(String name) {
        for (Mine mine : MINES) {
            if (mine.getName().equalsIgnoreCase(name)) {
                return mine;
            }
        }
        return null;
    }

    public boolean isPlayerInAMine(Player p) {
        return getMineWith(p) != null;
    }

    public Mine getMineWith(Player p) {
        for (Mine m : MINES) {
            if (m.contains(p)) {
                return m;
            }
        }
        return null;
    }

    public void sendInfo(CommandSender p, Mine m) {
        MineMeMessageManager mm = plugin.getMessageManager();
        List<String> info = mm.translateAll(m.getInfo(), m);
        mm.sendMessage(p, info.toArray(new String[info.size()]));
    }

    private void notifyBlockChange(Block b) {
        for (Mine mine : MINES) {
            if (mine.contains(b)) {
                if (!mine.wasAlreadyBroken(b)) {
                    mine.setBlockAsBroken(b);
                }
            }
        }
    }

    public boolean isPartOfMine(Block b) {
        for (Mine mine : MINES) {
            if (mine.contains(b)) {
                return true;
            }
        }
        return false;
    }

    public void saveAll() {
        for (Mine mine : MINES) {
            mine.save();
        }
    }

    /**
     * @author DDevil_
     */
    public static class WorldEditManager {

        private final MineManager manager;

        public WorldEditManager(MineManager manager) {
            this.manager = manager;
        }

        public class WorldEditLogger extends AbstractLoggingExtent {

            private final Actor actor;
            private final World world;
            private final MineManager manager;

            public Actor getActor() {
                return actor;
            }

            public WorldEditLogger(World world, Actor actor, Extent extent, MineManager manager) {
                super(extent);
                this.actor = actor;
                this.world = world;
                this.manager = manager;
            }

            @Override
            protected void onBlockChange(Vector position, BaseBlock newBlock) {
                Block block = new Location(Bukkit.getWorld(world.getName()), position.getBlockX(), position.getBlockY(), position.getBlockZ()).getBlock();
                manager.notifyBlockChange(block);
            }
        }

        @Subscribe
        public void wrapForLogging(EditSessionEvent event) {
            Actor actor = event.getActor();
            World world = event.getWorld();
            if (actor != null && actor.isPlayer()) {
                event.setExtent(new WorldEditLogger(world, actor, event.getExtent(), manager));
            }
        }
    }
}
