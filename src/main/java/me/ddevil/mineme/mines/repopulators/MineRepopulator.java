package me.ddevil.mineme.mines.repopulators;

/**
 *
 */
public interface MineRepopulator {
    void repopulate(OnRepopulateEndListener listener);

    public interface OnRepopulateEndListener {
        void onEnd();
    }
}
