/* 
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.mines.repopulators;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.storage.StorageRequest;
import me.ddevil.mineme.utils.RandomCollection;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;
import java.util.Map;

public class BasicMineRepopulator extends AsyncMineRepopulator {
    private final Mine m;
    private final MineMe mineMe;

    public BasicMineRepopulator(Mine m, MineMe mineMe) {
        this.m = m;
        this.mineMe = mineMe;
    }


    @Override
    protected void doRepopulation() {
        try {
            RepopulateMap map = new RepopulateMap(m);
            if (map.isEmpty()) {
                return;
            }
            new BukkitRunnable() {
                @Override
                public void run() {
                    ((Iterator<Block>) m.iterator()).forEachRemaining(b -> {
                        ItemStack bb = map.getRandomBlock();
                        b.setType(bb.getType());
                        b.setData(bb.getData().getData());
                    });
                }
            }.runTask(mineMe);
            mineMe.getStorageManager().addReset(new StorageRequest(m));

        } catch (Exception e) {
            mineMe.printException("There was an error repopulating mine " + m.getName() + ", is the composition correct?", e);
        }
    }

    private class RepopulateMap {

        private final RandomCollection<ItemStack> randomCollection;

        private RepopulateMap(Mine mine) {
            randomCollection = new RandomCollection<>();
            Map<ItemStack, Double> composition = mine.getComposition();
            for (ItemStack m : composition.keySet()) {
                randomCollection.add(
                        mine.getPercentage(m),
                        m);
            }

        }

        private boolean isEmpty() {
            return randomCollection.isEmpty();
        }

        private ItemStack getRandomBlock() {
            return randomCollection.next();
        }
    }
}
