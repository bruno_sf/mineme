package me.ddevil.mineme.mines.repopulators;

import me.ddevil.core.thread.CustomThread;

/**
 * Created by BRUNO II on 10/08/2016.
 */
public abstract class AsyncMineRepopulator extends CustomThread implements MineRepopulator {
    private OnRepopulateEndListener listener;

    @Override
    public void doRun() {
        doRepopulation();
        listener.onEnd();
    }

    protected abstract void doRepopulation();

    @Override
    public void repopulate(OnRepopulateEndListener listener) {
        this.listener = listener;
        start();
    }
}
