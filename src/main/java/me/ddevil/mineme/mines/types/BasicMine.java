/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.mines.types;

import com.sk89q.worldedit.regions.Region;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.Challenge;
import me.ddevil.mineme.challenges.ChallengeParameters;
import me.ddevil.mineme.configuration.MineMeConfiguration;
import me.ddevil.mineme.events.mines.MineBlockBreakEvent;
import me.ddevil.mineme.events.mines.MineResetEvent;
import me.ddevil.mineme.events.mines.MineUpdateEvent;
import me.ddevil.mineme.events.players.PlayerEnterMineEvent;
import me.ddevil.mineme.events.players.PlayerLeaveMineEvent;
import me.ddevil.mineme.holograms.CompatibleHologram;
import me.ddevil.mineme.mines.*;
import me.ddevil.mineme.mines.repopulators.BasicMineRepopulator;
import me.ddevil.mineme.mines.repopulators.MineRepopulator;
import me.ddevil.mineme.regions.MineRegion;
import me.ddevil.mineme.regions.MineRegionManager;
import me.ddevil.mineme.storage.StorageManager;
import me.ddevil.mineme.task.MineStatsRecalculator;
import me.ddevil.shiroi.config.ConstantsConfigurationManager;
import me.ddevil.shiroi.misc.DebugLevel;
import me.ddevil.shiroi.utils.item.ItemUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public abstract class BasicMine<R extends Region> implements Mine<R> {

    //General
    protected final String name;
    protected final MineMe plugin;
    protected final MineManager mineManager;
    private final String customBroadcast;
    protected final ConstantsConfigurationManager<?> configurationManager;
    protected R area;
    protected boolean useCustomBroadcast;
    protected String alias;
    protected final FileConfiguration config;
    protected File saveFile;
    protected boolean enabled = true;
    //Messages
    protected boolean broadcastOnReset;
    protected String broadcastMessage;
    protected double broadcastRadius;
    protected boolean broadcastNearby;

    //Blocks
    protected Map<ItemStack, Double> composition;
    protected final ArrayList<Block> brokenBlocks = new ArrayList();
    protected final ArrayList<Block> lastSecond = new ArrayList();
    protected final HashMap<UUID, List<Block>> playerSpecificStatus = new HashMap();
    //Clock
    protected int currentResetDelay;
    protected int totalResetDelay;
    protected boolean deleted = false;
    protected ItemStack icon;

    //Potions
    protected boolean useEffects;
    protected final List<PotionEffect> potionEffects;

    //Challenges
    protected boolean allowChallenges;
    protected boolean allowChallengesOverrideHolograms;
    protected double challengeChance;
    protected List<ChallengeParameters> challengeParameters;
    protected Challenge currentChallenge;
    protected ArrayList<Challenge> challengeQueue = new ArrayList();

    public BasicMine(MineMe plugin, final MineConfig config) {
        //General me.ddevil.config
        this.enabled = true;
        this.plugin = plugin;
        this.name = config.getName();
        this.alias = plugin.getMessageManager().translateColors(config.getAlias());
        this.world = config.getWorld();
        this.composition = config.getComposition();
        this.config = config.getConfig();
        this.saveFile = plugin.getMineFile(this);
        this.mineManager = plugin.getMineManager();
        this.allowChallengesOverrideHolograms = config.allowChallengesOverrideHolograms();
        //Resets
        this.totalResetDelay = config.getResetDelay() * 60;
        this.currentResetDelay = totalResetDelay;
        //Broadcasts
        this.configurationManager = plugin.getConfigManager();
        this.broadcastOnReset = config.isBroadcastOnReset();
        this.useCustomBroadcast = config.isUseCustomBroadcast();
        this.customBroadcast = config.getBroadcastMessage();
        if (configurationManager.getValue(MineMeConfiguration.FORCE_DEFAULT_BROADCAST_MESSAGE)||!useCustomBroadcast) {
            this.broadcastMessage = configurationManager.getValue(MineMeConfiguration.DEFAULT_RESET_MESSAGE, plugin.getMessagesConfig());
        } else {
            this.broadcastMessage = customBroadcast;

        }
        //Nearby broadcast
        this.broadcastNearby = config.isNearbyBroadcast();
        this.broadcastRadius = config.getBroadcastRadius();
        //Effects
        this.useEffects = config.useEffects();
        this.potionEffects = config.getEffects();
        //Icon
        this.icon = plugin.getMessageManager().createIcon(config.getConfig().getConfigurationSection("icon"), this);

        //Load challenges
        this.allowChallenges = config.isChallengesEnabled();
        this.challengeChance = config.getChallengeChance();
        this.challengeParameters = new ArrayList();
        for (String s : config.getChallenges()) {
            String[] split = s.split(":");
            int length = split.length;
            if (length == 0) {
                continue;
            }
            String name = split[0];
            int duration;
            if (length > 1) {
                try {
                    duration = Integer.valueOf(split[1]);
                } catch (NumberFormatException e) {
                    plugin.debug("There was an error trying to get challenge duration from " + split[1] + ", using 2 as duration.", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                    duration = 2;
                }
            } else {
                duration = 2;
            }
            int amplifier;
            if (length > 2) {
                try {
                    amplifier = Integer.valueOf(split[2]);
                } catch (NumberFormatException e) {
                    plugin.debug("There was an error trying to get challenge amplifier from " + split[1] + ", using 0 as amplifier.", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                    amplifier = 0;
                }
            } else {
                amplifier = 0;
            }
            this.challengeParameters.add(new ChallengeParameters(this, name, duration, amplifier));
        }
    }

    /**
     * Creates a new mine with the default settings
     *
     * @param plugin
     * @param name   The name of the mine
     * @param world  The world of the mine
     * @param icon   The icon of the mine
     */
    public BasicMine(MineMe plugin, String name, World world, ItemStack icon) {
        this.plugin = plugin;
        //General
        this.enabled = true;
        this.name = name;
        this.alias = name;
        this.world = world;
        this.composition = new HashMap();
        this.composition.put(new ItemStack(Material.STONE), 100d);
        //Broadcast
        this.broadcastOnReset = true;
        this.broadcastRadius = 50;
        this.broadcastMessage = plugin.getConfigManager().getValue(MineMeConfiguration.DEFAULT_RESET_MESSAGE, plugin.getMessagesConfig());
        this.broadcastNearby = false;
        //Resets
        this.totalResetDelay = 300;
        this.currentResetDelay = totalResetDelay;
        this.icon = icon;
        this.config = plugin.getYAMLMineFile(this);
        this.potionEffects = new ArrayList();
        this.saveFile = plugin.getMineFile(this);
        this.mineManager = plugin.getMineManager();
        this.configurationManager = plugin.getConfigManager();
        this.allowChallenges = true;
        this.challengeParameters = new ArrayList();
        this.useCustomBroadcast = false;
        this.customBroadcast = "%prefix%%separator%Hallo, I'm a custom broadcast, I like bananas";
    }

    @Override
    public R getWERegion() {
        return area;
    }

    @Override
    public int getLowerY() {
        return area.getMinimumPoint().getBlockY();
    }

    @Override
    public int getUpperY() {
        return area.getMaximumPoint().getBlockY();
    }

    @Override
    public int getVolume() {
        return area.getArea();
    }

    @Override
    public String getName() {
        return name;
    }

    public void setIcon(ItemStack icon) {
        this.icon = icon;
    }

    @Override
    public ItemStack getIcon() {
        return icon;
    }

    public int getResetMinutesDelay() {
        return totalResetDelay / 60;
    }

    public void setResetMinutesDelay(int resetMinutesDelay) {
        this.totalResetDelay = resetMinutesDelay * 60;
        save();
    }

    @Override
    public boolean broadcastOnReset() {
        return broadcastOnReset;
    }

    @Override
    public boolean broadcastToNearbyOnly() {
        return broadcastNearby;
    }

    @Override
    public double getBroadcastRadius() {
        return broadcastRadius;
    }

    @Override
    public boolean isBroadcastOnReset() {
        return broadcastOnReset;
    }

    @Override
    public void setBroadcastOnReset(boolean broadcastOnReset) {
        this.broadcastOnReset = broadcastOnReset;
        save();

    }

    @Override
    public void setResetDelay(int resetDelay) {
        this.totalResetDelay = resetDelay * 60;
        save();
        reset();
    }

    public void setNearbyBroadcast(boolean nearbyBroadcast) {
        this.broadcastNearby = nearbyBroadcast;
        save();
    }

    @Override
    public void setBroadcastRange(double broadcastRadius) {
        this.broadcastRadius = broadcastRadius;
        save();
    }

    public void setBroadcastMessage(String broadcastMessage) {
        this.broadcastMessage = broadcastMessage;
        save();
    }


    public String getBroadcastMessage() {
        return broadcastMessage;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    @Override
    public void secondCountdown() {
        currentResetDelay--;
        if (useEffects) {
            for (PotionEffect e : getEffects()) {
                for (Player p : getPlayersInside()) {
                    p.addPotionEffect(e, true);
                }
            }
        }
        if (currentResetDelay <= 0) {
            reset();
        }
        lastSecond.clear();
    }

    @Override
    public int averageBreakSpeed() {
        return lastSecond.size();
    }

    @Override
    public List<String> getInfo() {
        String[] basic = new String[]{
                "%header%",
                "$3Name: $2" + getName(),
                "$3Alias: $2" + getAlias(),
                "$3Type: $2" + getType(),
                "$3World: $2" + getCenter().getWorld().getName(),
                "$3Location: $2" + getCenter().getBlockX() + ", " + getCenter().getBlockY() + ", " + getCenter().getBlockZ() + ", ",
                "$3Broadcast on reset: $2" + broadcastOnReset(),
                "$3Nearby broadcast: $2" + broadcastToNearbyOnly(),
                "$3Broadcast radius: $2" + getBroadcastRadius(),
                "$3Composition: $2"
        };
        ArrayList<String> comp = new ArrayList();
        comp.addAll(Arrays.asList(basic));
        for (ItemStack m : getMaterials()) {
            comp.add("$3" + m.getType() + "$1:$3" + m.getData().getData() + " $2= $1" + composition.get(m));
        }
        return comp;
    }

    @Override
    public boolean wasAlreadyBroken(Block b) {
        return brokenBlocks.contains(b);
    }

    @Override
    public float getPercentageMined() {
        if (brokenBlocks.isEmpty()) {
            return 0;
        } else {
            double percentage = (brokenBlocks.size() * 100f) / getVolume();
            return Math.round(percentage);
        }
    }

    @Override
    public double getPercentage(ItemStack m) {
        if (MineUtils.containsRelativeItemStackInComposition(this, m)) {
            m = MineUtils.getItemStackInComposition(this, m);
            for (ItemStack i : composition.keySet()) {
                if (ItemUtils.equalMaterial(i, m)) {
                    if (composition.get(i) == null) {
                        return 0;
                    }
                    return composition.get(i);
                }
            }
            return 0;
        } else {
            return 0;
        }

    }

    @Override
    public float getPercentageRemaining() {
        if (getRemainingBlocks() <= 0) {
            return 0;
        } else {
            double percentage = (getRemainingBlocks() * 100) / getVolume();
            return Math.round(percentage);
        }
    }

    @Override
    public int getMinedBlocks() {
        return getVolume() - getRemainingBlocks();
    }

    @Override
    public int getRemainingBlocks() {
        return getVolume() - brokenBlocks.size();
    }

    @Override
    public List<Player> getPlayersInside() {
        return playersInside;
    }

    public void updatePlayers() {
        for (Player p : world.getPlayers()) {
            if (contains(p)) {
                if (!playersInside.contains(p)) {
                    PlayerEnterMineEvent event = new PlayerEnterMineEvent(p, this).call();
                    if (event.isCancelled()) {

                    } else {
                        playersInside.add(p);
                    }
                }
            } else if (playersInside.contains(p)) {
                new PlayerLeaveMineEvent(p, this).call();
            }
        }
    }

    ArrayList<Player> playersInside = new ArrayList();

    @EventHandler
    public void onBreak(MineBlockBreakEvent e) {
        if (!enabled) {
            return;
        }
        Block b = e.getBlock();
        if (contains(b)) {
            if (!e.isCancelled()) {
                setBlockAsBroken(b, e.getPlayer().getUniqueId());
            } else {
                plugin.debug("Block break event for mine " + name + " was cancelled", DebugLevel.NO_BIG_DEAL);
            }
        }
    }

    private void setBlockAsBroken(Block block, UUID uuid) {
        if (!enabled) {
            return;
        }
        if (!contains(block)) {
            return;
        }
        brokenBlocks.add(block);
        lastSecond.add(block);
        List<Block> blocks = playerSpecificStatus.get(uuid);
        if (blocks == null) {
            blocks = new ArrayList();
        }
        blocks.add(block);
        playerSpecificStatus.put(uuid, blocks);
        new MineUpdateEvent(this).call();
        if (this instanceof HologramCompatibleMine) {
            HologramCompatibleMine hc = (HologramCompatibleMine) this;
            hc.softHologramUpdate();
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (!enabled) {
            return;
        }
        Block b = e.getBlock();
        if (contains(b)) {
            if (!wasAlreadyBroken(b)) {
                new MineBlockBreakEvent(this, b, e.getPlayer(), e).call();
            }
        }
    }

    @EventHandler
    public void onBreak(BlockExplodeEvent e) {
        if (!enabled) {
            return;
        }
        Block b = e.getBlock();
        if (contains(b)) {
            if (!wasAlreadyBroken(b)) {
                new MineBlockBreakEvent(this, b, null, e).call();
            }
        }
    }

    @EventHandler
    public void onBreak(EntityExplodeEvent e) {
        if (!enabled) {
            return;
        }
        e.blockList().stream().filter(b -> contains(b)).filter(b -> !wasAlreadyBroken(b)).forEach(b -> {
            new MineBlockBreakEvent(this, b, null, e).call();
        });
        new MineUpdateEvent(this).call();
        if (this instanceof HologramCompatibleMine) {
            HologramCompatibleMine hc = (HologramCompatibleMine) this;
            hc.softHologramUpdate();
        }
    }

    @Override
    public void setBlockAsBroken(Block block) {
        if (!enabled) {
            return;
        }
        if (!contains(block)) {
            return;
        }
        brokenBlocks.add(block);
        lastSecond.add(block);
        new MineUpdateEvent(this).call();
        if (this instanceof HologramCompatibleMine) {
            HologramCompatibleMine hc = (HologramCompatibleMine) this;
            hc.softHologramUpdate();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean containsMaterial(ItemStack material) {
        return MineUtils.containsRelativeItemStackInComposition(this, material);
    }

    @Override
    public void removeMaterial(ItemStack material) {
        material = MineUtils.getItemStackInComposition(this, material);
        boolean found = false;
        for (ItemStack i : getMaterials()) {
            if (i.equals(material)) {
                composition.remove(i);
                found = true;
                break;
            }
        }
        if (!found) {
            plugin.debug("Tried to remove material "
                    + material.getType() + ":"
                    + material.getData().getData() + " from mine "
                    + name + ", but it isn't a part of the composition.", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
        }
        save();
    }

    @Override
    public double getFreePercentage() {
        if (isExceedingMaterials()) {
            return 0;
        }
        return 100 - getTotalMaterialPercentage();
    }

    @Override
    public double getExceedingTotal() {
        double exceeding = getTotalMaterialPercentage() - 100;
        if (exceeding > 0) {
            return exceeding;
        }
        return 0;

    }

    @Override
    public void setMaterialPercentage(ItemStack material, double percentage) {
        material = getRelativeItemStackInComposition(material);
        if (material == null) {
            return;
        }
        if (percentage < 0) {
            percentage = 0;
        }
        plugin.debug("The percentage of " + material.getType() + ":" + material.getData().getData() + " of mine " + name + " was changed to " + percentage + ".", DebugLevel.NO_BIG_DEAL);
        composition.put(material, percentage);
        save();
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public Map<ItemStack, Double> getComposition() {
        return composition;
    }

    @Override
    public void setComposition(Map<ItemStack, Double> composition) {
        this.composition = composition;
    }

    /**
     * Get the Mine's world.
     *
     * @return The World object representing this Mine's world
     */
    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public List<ItemStack> getMaterials() {
        ArrayList<ItemStack> s = new ArrayList<>();
        s.addAll(composition.keySet());
        return s;
    }

    protected final World world;

    @Override
    public String toString() {
        JSONObject obj = new JSONObject();
        obj.put("name", name);
        obj.put("location", getCenter().toString());
        return obj.toJSONString();
    }

    @Override
    public int getTimeToNextReset() {
        return currentResetDelay;
    }

    //Challenges


    @Override
    public Challenge getCurrentChallenge() {
        return currentChallenge;
    }

    @Override
    public void addChallengeToQueue(Challenge challenge) {
        challengeQueue.add(challenge);
        updateChallenges();
    }

    protected void updateChallenges() {
        if (currentChallenge == null) {
            if (!challengeQueue.isEmpty()) {
                Challenge challenge = challengeQueue.get(0);
                challengeQueue.remove(0);
                currentChallenge = challenge;
                currentChallenge.start();
                plugin.getMessageManager().broadcastMessage("A " + challenge.getName() + " challenge started in mine " + alias + "!");
                currentChallenge.addListener(result -> {
                    currentChallenge = null;
                    updateChallenges();
                });
            }
        }
    }

    @Override
    public void forceSetCurrentChallenge(Challenge challenge) {
        if (currentChallenge != null) {
            currentChallenge.complete(null, null);
        }
        currentChallenge = challenge;
        currentChallenge.start();
        currentChallenge.addListener(result -> {
            currentChallenge = null;
            updateChallenges();
        });
    }

    @Override
    public void setAlias(String alias) {
        this.alias = alias;
        save();
    }

    @Override
    public void clear() {
        for (Block b : this) {
            b.setType(Material.AIR);
        }
    }

    @Override
    public void fill(ItemStack item) {
        for (Block b : this) {
            b.setType(item.getType());
            b.setData(item.getData().getData());
        }
    }

    @Override
    public void disable() {
        clear();
        setEnabled(false);
        if (this instanceof HologramCompatibleMine) {
            HologramCompatibleMine hc = (HologramCompatibleMine) this;
            hc.getHolograms().forEach(CompatibleHologram::delete);
        }
        mineManager.unregisterMine(this);
        save();
    }

    @Override
    public void delete() {
        clear();
        setEnabled(false);
        saveFile.delete();
        mineManager.unregisterMine(this);
        deleted = true;
        if (this instanceof HologramCompatibleMine) {
            HologramCompatibleMine hc = (HologramCompatibleMine) this;
            hc.getHolograms().forEach(CompatibleHologram::delete);
        }
    }


    @Override
    public double getTotalMaterialPercentage() {
        if (composition == null) {
            return 0;
        }
        double total = 0;
        for (double i : composition.values()) {
            total += i;
        }
        return total;
    }

    @Override
    public int getTotalMaterials() {
        if (composition == null) {
            return 0;
        }
        return composition.keySet().size();
    }


    @Override
    public boolean useEffects() {
        return useEffects;
    }

    @Override
    public List<PotionEffect> getEffects() {
        return potionEffects;
    }

    @Override
    public void addPotionEffect(PotionEffect effect) {
        PotionEffectType type = effect.getType();
        if (getEffect(type) != null) {
            removePotionEffect(type);
        }
        potionEffects.add(effect);

    }

    @Override
    public void removePotionEffect(PotionEffectType effectType) {
        for (Iterator<PotionEffect> it = potionEffects.iterator(); it.hasNext(); ) {
            PotionEffect e = it.next();
            if (e.getType().equals(effectType)) {
                it.remove();
            }
        }
    }

    @Override
    public void clearEffects() {
        potionEffects.clear();
    }


    @Override
    public int getResetTime() {
        return totalResetDelay;
    }

    //Config
    public FileConfiguration getBasicSavedConfig() {
        FileConfiguration c = plugin.getYAMLMineFile(this);
        c.set("enabled", enabled);
        c.set("name", name);
        c.set("alias", alias);
        c.set("world", world.getName());
        c.set("type", getType().name());
        //Composition
        ArrayList<String> comp = new ArrayList();
        composition.keySet().forEach(itemStack -> {
            String s = itemStack.getType().name() + ":" + itemStack.getData().getData();
            comp.add(s + "=" + composition.get(itemStack));
        });
        c.set("composition", comp);
        //Resets
        if (!c.contains("reset")) {
            c.createSection("reset");
        }
        ConfigurationSection reset = c.getConfigurationSection("reset");
        reset.set("delay", getResetMinutesDelay());
        //Broadcasts
        if (!reset.contains("broadcast")) {
            reset.createSection("broadcast");
        }
        ConfigurationSection broadcast = reset.getConfigurationSection("broadcast");
        broadcast.set("use", broadcastOnReset);
        broadcast.set("nearbyOnly", broadcastNearby);
        broadcast.set("nearbyRadius", broadcastRadius);
        if (!broadcast.contains("custom")) {
            broadcast.createSection("custom");
        }
        ConfigurationSection custom = broadcast.getConfigurationSection("custom");
        custom.set("use", useCustomBroadcast);
        custom.set("customBroadcast", customBroadcast);
        if (!c.contains("icon")) {
            c.createSection("icon");
            c.set("icon.type", icon.getType().toString());
            c.set("icon.data", icon.getData().getData());
            c.set("icon.name", icon.getItemMeta().getDisplayName());
            c.set("icon.lore", ItemUtils.getLore(icon));
        }
        //Challenges
        if (!c.contains("challenges")) {
            c.createSection("challenges");
        }
        ConfigurationSection challenges = c.getConfigurationSection("challenges");
        challenges.set("enabled", allowChallenges);
        challenges.set("chance", challengeChance * 100);
        List<String> challengesList = challengeParameters.stream()
                .map(challengeParameters1 -> challengeParameters1.getChallengeName() + ":" + challengeParameters1.getChallengeDuration() + ":" + challengeParameters1.getChallengeAmplifier())
                .collect(Collectors.toList());
        challenges.set("list", challengesList);
        //Effects
        List<String> effectList = potionEffects.stream().map(e -> e.getType().getName() + ":" + (e.getDuration() / 20) + ":" + e.getAmplifier()).collect(Collectors.toList());
        if (!c.contains("effects")) {
            c.createSection("effects");
        }
        ConfigurationSection effects = c.getConfigurationSection("effects");
        effects.set("use", useEffects);
        effects.set("list", effectList);
        //Holograms
        if (this instanceof HologramCompatibleMine) {
            HologramCompatibleMine hc = (HologramCompatibleMine) this;
            if (!c.contains("holograms")) {
                c.createSection("holograms");
            }
            ConfigurationSection holograms = c.getConfigurationSection("holograms");
            holograms.set("useCustomText", hc.useCustomHologramText());
            holograms.set("customText", hc.getHologramsLines());
        }
        return c;
    }

    public void reset(MineRepopulator repopulator) {
        if (isDeleted() || !isEnabled()) {
            return;
        }
        MineResetEvent event = new MineResetEvent(this).call();
        if (!event.isCancelled()) {
            plugin.debug("Reseting mine " + name, DebugLevel.NO_BIG_DEAL);
            //Pull players up
            for (Player p : getPlayersInside()) {
                Location l = p.getLocation();
                l.setY(getUpperY() + 2);
                p.teleport(l);
            }
            currentResetDelay = totalResetDelay;
            repopulator.repopulate(() -> {
                if (broadcastOnReset) {
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        if (broadcastNearby) {
                            if (p.getLocation().getWorld() == getCenter().getWorld()) {
                                if (p.getLocation().distance(getCenter()) <= broadcastRadius) {
                                    p.sendMessage(
                                            plugin.getMessageManager().translateAll(
                                                    broadcastMessage,
                                                    this));
                                }
                            }
                        } else {
                            p.sendMessage(plugin.getMessageManager().translateAll(broadcastMessage, this));
                        }
                    }
                }
                //Test for challenge
                if (allowChallenges && !challengeParameters.isEmpty()) {
                    Random random = new Random();
                    if (random.nextDouble() <= challengeChance) {
                        ChallengeParameters challengeParameters = this.challengeParameters.get(random.nextInt(this.challengeParameters.size()));
                        Challenge challenge;
                        try {
                            challenge = plugin.getChallengeManager().requestChallenge(challengeParameters);
                            addChallengeToQueue(challenge);
                        } catch (IllegalArgumentException challengee) {
                        }
                    }
                }
                brokenBlocks.clear();
                playerSpecificStatus.clear();
            });

        } else {
            plugin.debug("Reset event for mine " + name + " was cancelled", DebugLevel.NO_BIG_DEAL);
        }
    }

    @Override
    public void reset() {
        reset(new BasicMineRepopulator(this, plugin));
    }

    @Override
    public void clearMaterials() {
        composition.clear();
    }

    @Override
    public ItemStack getRelativeItemStackInComposition(ItemStack item) {
        if (item != null) {
            for (ItemStack a : composition.keySet()) {
                if (a.getType() == item.getType()) {
                    if (a.getData().getData() == item.getData().getData()) {
                        return a;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public boolean containsRelativeItemStackInComposition(ItemStack i) {
        return getRelativeItemStackInComposition(i) != null;
    }

    @Override
    public void addMaterial(ItemStack material) {
        if (!containsRelativeItemStackInComposition(material)) {
            setMaterialPercentage(material, 1);
        }
    }

    @Override
    public PotionEffect getEffect(PotionEffectType type) {

        for (PotionEffect effect : getEffects()) {
            if (effect.getType().equals(type)) {
                return effect;
            }
        }
        return null;
    }

    protected abstract FileConfiguration getConfigurationFile();

    @Override
    public final void save() {
        FileConfiguration file = getConfigurationFile();
        try {
            file.save(plugin.getMineFile(this));
        } catch (IOException ex) {
            plugin.printException("There was an error while saving " + name + "'s me.ddevil.config", ex);
        }
    }

    @Override
    public long getLifetimeBrokenBlocks() throws Exception {
        return plugin.getStorageManager().getMineInfo(this).getTotalBrokenBlocks();
    }

    @Override
    public long getLifetimeResets() throws Exception {
        return plugin.getStorageManager().getMineInfo(this).getTotalResets();
    }

    @Override
    public MineRegion getRegion() {
        MineRegionManager regionManager = plugin.getRegionManager();
        if (regionManager != null) {
            return regionManager.getRegion(this);
        }
        return null;
    }

    @Override
    public Map<UUID, List<Block>> getPlayersLastBrokenBlocks() {
        return playerSpecificStatus;
    }

    @Override
    public boolean useCustomBroadcast() {
        return useCustomBroadcast;
    }

    @Override
    public void setUseCustomBroadcast(boolean useCustomBroadcast) {
        this.useCustomBroadcast = useCustomBroadcast;
    }

    @Override
    public long getTotalBlocksBroken(Player player) {
        StorageManager storageManager = plugin.getStorageManager();
        try {
            return storageManager.getPlayerInfo(player).getBrokenBlocks(this);
        } catch (Exception e) {
            return -1l;
        }
    }

    @Override
    public boolean useChallenges() {
        return allowChallenges;
    }

    @Override
    public boolean allowChallengesOverrideHolograms() {
        return allowChallengesOverrideHolograms;
    }

    @Override
    public void setAllowChallengesOverrideHolograms(boolean allowChallengesOverrideHolograms) {
        this.allowChallengesOverrideHolograms = allowChallengesOverrideHolograms;
    }
    @Override
    public List<ChallengeParameters> getChallengeParameters() {
        return challengeParameters;
    }
    @Override
    public MineStatsRecalculator recalculateStatistics() {
        MineStatsRecalculator customThread = new MineStatsRecalculator(this, plugin.getStorageManager());
        return customThread;
    }


}
