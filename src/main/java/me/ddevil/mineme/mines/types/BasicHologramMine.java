/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.mines.types;

import com.sk89q.worldedit.regions.Region;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.configuration.MineMeConfiguration;
import me.ddevil.mineme.events.mines.MineHologramUpdateEvent;
import me.ddevil.mineme.holograms.CompatibleHologram;
import me.ddevil.mineme.holograms.HologramManager;
import me.ddevil.mineme.mines.HologramCompatibleMine;
import me.ddevil.mineme.mines.MineConfig;
import me.ddevil.shiroi.misc.DebugLevel;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DDevil_
 */
public abstract class BasicHologramMine<R extends Region> extends BasicMine<R> implements HologramCompatibleMine {

    protected final HologramManager hologramManager;
    protected boolean hologramsReady = false;
    protected final boolean useCustomHologramText;

    public BasicHologramMine(MineMe plugin, String name, World world, ItemStack icon) {
        super(plugin, name, world, icon);
        useCustomHologramText = false;
        this.hologramManager = plugin.getHologramManager();
    }

    public BasicHologramMine(MineMe plugin, MineConfig config) {
        super(plugin, config);
        useCustomHologramText = config.getConfig().getBoolean("useCustomHologramText");
        hologramManager = plugin.getHologramManager();
    }

    protected final List<Integer> tagLines = new ArrayList<>();

    @Override
    public void setupHolograms() {
        //Load hologram pattern
        if (configurationManager.getValue(MineMeConfiguration.HOLOGRAMS_FORCE_DEFAULT_TEXT) || !config.getBoolean("useCustomHologramText")) {
            plugin.debug("Setting default hologram text for mine " + name + " because forceDefaultHologramOnAllMines is enabled on the me.ddevil.config");
            setHologramsLines(configurationManager.getValue(MineMeConfiguration.HOLOGRAMS_DEFAULT_TEXT));
        } else {
            plugin.debug("Setting custom hologram text for mine " + name);
            setHologramsLines(config.getStringList("hologramsText"));
        }
        //Detect placeholder lines
        for (int i = 0; i < getHologramsLines().size(); i++) {
            String text = getHologramsLines().get(i);
            if (text.contains("%") && !text.equalsIgnoreCase("%item%")) {
                tagLines.add(i);
            }
        }
        //Place holograms
        placeHolograms();
        //Inicial holograms setup
        int totallines = getHologramsLines().size();
        for (CompatibleHologram h : getHolograms()) {
            h.move(h.getLocation().clone().add(0, 0.3 * totallines + 1.3, 0));
            h.clearLines();
            for (String text : getHologramsLines()) {
                if (text.equalsIgnoreCase("%icon%")) {
                    h.appendItemLine(icon);
                } else {
                    h.appendTextLine(plugin.getMessageManager().translateAll(text, this));
                }
            }
        }
        //Update
        updateHolograms();
    }

    public abstract void placeHolograms();

    @Override
    public void showHolograms() {
        updateHolograms();
    }

    @Override
    public void hideHolograms() {
        for (CompatibleHologram m : getHolograms()) {
            m.clearLines();
        }
    }

    private Integer lightHologramUpdateId;

    @Override
    public void softHologramUpdate() {
        if (lightHologramUpdateId == null) {
            plugin.debug("Updating hologram softly.");
            lightHologramUpdateId = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                @Override
                public void run() {
                    updateHolograms();
                    lightHologramUpdateId = null;
                }
            }, 60l);
        }
    }

    @Override
    public boolean isHologramsVisible() {
        return hologramsReady;
    }

    @Override
    public void updateHolograms() {
        if (isDeleted()) {
            return;
        }
        if (getHolograms().isEmpty()) {
            return;
        }
        MineHologramUpdateEvent event = (MineHologramUpdateEvent) new MineHologramUpdateEvent(this).call();
        if (!event.isCancelled()) {
            for (CompatibleHologram h : getHolograms()) {
                for (int i : tagLines) {
                    String text = getHologramsLines().get(i);
                    h.setLine(i, plugin.getMessageManager().translateAll(text, this));
                }
            }
        } else {
            plugin.debug("Hologram Update Event for mine " + name + " was cancelled", DebugLevel.NO_BIG_DEAL);
        }
    }

    @Override
    public boolean useCustomHologramText() {
        return useCustomHologramText;
    }

    protected final ArrayList<CompatibleHologram> holograms = new ArrayList<>();
    protected List<String> hologramsLines;

    @Override
    public List<CompatibleHologram> getHolograms() {
        return holograms;
    }

    @Override
    public List<String> getHologramsLines() {
        return hologramsLines;
    }

    @Override
    public void setHologramsLines(List<String> lines) {
        this.hologramsLines = lines;
    }

    @Override
    public void reset() {
        super.reset();
        if (configurationManager.getValue(MineMeConfiguration.HOLOGRAMS_ENABLED)) {
            updateHolograms();
        }
    }

}
