/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.mines.types;

import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.selections.Polygonal2DSelection;
import com.sk89q.worldedit.regions.Polygonal2DRegion;
import com.sk89q.worldedit.regions.Region;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.MineType;
import me.ddevil.mineme.mines.MineConfig;
import me.ddevil.mineme.utils.WorldEditIterator;
import me.ddevil.shiroi.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author DDevil_
 */
public class PolygonMine extends BasicHologramMine<Polygonal2DRegion> {

    private final List<BlockVector2D> points;
    private final int height;

    public PolygonMine(MineMe plugin, MineConfig config) {
        super(plugin, config);
        FileConfiguration fileConfig = config.getConfig();
        ConfigurationSection pointsConfig = fileConfig.getConfigurationSection("points");
        ArrayList<BlockVector2D> vectors = new ArrayList();
        for (String pointsPath : pointsConfig.getKeys(false)) {
            String pointPath = "points." + pointsPath;
            int x = fileConfig.getInt(pointPath + ".x");
            int z = fileConfig.getInt(pointPath + ".z");
            BlockVector2D bv = new BlockVector2D(x, z);
            vectors.add(bv);
        }
        int lowestPoint = fileConfig.getInt("minY");
        int hightestPoint = fileConfig.getInt("maxY");
        this.area = new Polygonal2DRegion(new BukkitWorld(world), vectors, lowestPoint, hightestPoint);
        this.points = vectors;
        this.height = hightestPoint - lowestPoint;
    }

    public PolygonMine(MineMe plugin, Vector[] points, String name, World world, ItemStack icon) {
        super(plugin, name, world, icon);
        this.points = new ArrayList(Arrays.asList(points));
        ArrayList<BlockVector2D> bvectors = new ArrayList();
        Integer lowestPoint = null;
        Integer hightestPoint = null;

        for (Vector point : points) {
            BlockVector2D bv = new BlockVector2D(point.getX(), point.getZ());
            bvectors.add(bv);
            if (lowestPoint == null) {
                lowestPoint = point.getBlockY();
            } else if (point.getBlockY() < lowestPoint) {
                lowestPoint = point.getBlockY();
            }
            if (hightestPoint == null) {
                hightestPoint = point.getBlockY();
            } else if (point.getBlockY() > hightestPoint) {
                hightestPoint = point.getBlockY();
            }
        }
        this.area = new Polygonal2DRegion(new BukkitWorld(world), bvectors, lowestPoint, hightestPoint);
        this.height = hightestPoint - lowestPoint;

    }

    public PolygonMine(MineMe plugin, Polygonal2DRegion region, String name, World world) {
        super(plugin, name, world, new ItemBuilder(Material.REDSTONE_BLOCK, plugin).setName(name).toItemStack());
        this.points = region.getPoints();
        this.area = region;
        this.height = region.getHeight();

    }

    public PolygonMine(MineMe plugin, Polygonal2DSelection region, String name, World world) {
        this(plugin,
                new Polygonal2DRegion(
                        new BukkitWorld(region.getWorld()),
                        region.getNativePoints(),
                        region.getNativeMinimumPoint().getBlockY(),
                        region.getNativeMaximumPoint().getBlockY()),
                name, world);
    }

    @Override
    public void placeHolograms() {
        for (BlockVector2D point : points) {
            holograms.add(
                    hologramManager.createHologram(
                            new Location(world, point.getX(), getUpperY(), point.getZ()))
            );
        }
    }

    @Override
    protected FileConfiguration getConfigurationFile() {
        FileConfiguration file = getBasicSavedConfig();
        file.set("minY", area.getMinimumY());
        file.set("maxY", area.getMaximumY());
        int i = 0;
        for (BlockVector2D v : points) {
            String point = "points.point" + i;
            file.set(point + ".x", v.getBlockX());
            file.set(point + ".z", v.getBlockZ());
            i++;
        }
        return file;
    }

    @Override
    public MineType getType() {
        return MineType.POLYGON;
    }

    @Override
    public boolean contains(double x, double y, double z) {
        boolean downGrade = y <= getUpperY() + 1 && y >= getUpperY();
        com.sk89q.worldedit.Vector vector = new com.sk89q.worldedit.Vector(
                x,
                downGrade ? y - 1 : y,
                z);
        return area.contains(vector);
    }

    @Override
    public Location getCenter() {
        com.sk89q.worldedit.Vector center = area.getCenter();
        return new Location(world, center.getX(), getUpperY() - height / 2, center.getZ());
    }

    @Override
    public int getLowerY() {
        return area.getMinimumY();
    }

    @Override
    public int getUpperY() {
        return area.getMaximumY();
    }

    @Override
    public int getVolume() {
        if (area == null) {
            return 0;
        }
        return area.getArea();
    }

    @Override
    public Iterator<Block> iterator() {
        return new WorldEditIterator(area);
    }

    public Polygonal2DRegion getWERegion() {
        return area;
    }
}
