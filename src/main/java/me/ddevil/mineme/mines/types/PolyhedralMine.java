package me.ddevil.mineme.mines.types;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.regions.ConvexPolyhedralRegion;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.MineConfig;
import me.ddevil.mineme.mines.MineType;
import me.ddevil.mineme.utils.WorldEditIterator;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 * Created by BRUNO II on 20/08/2016.
 */
public class PolyhedralMine extends BasicHologramMine<ConvexPolyhedralRegion> {

    public PolyhedralMine(MineMe plugin, MineConfig config) {
        super(plugin, config);
        this.area = new ConvexPolyhedralRegion(new BukkitWorld(world));
        FileConfiguration config1 = config.getConfig();
        if (config1.contains("points")) {
            ConfigurationSection points = config1.getConfigurationSection("points");
        }
    }

    public PolyhedralMine(MineMe plugin, String name, World world, ItemStack icon) {
        super(plugin, name, world, icon);
        this.area = new ConvexPolyhedralRegion(new BukkitWorld(world));
    }


    @Override
    public void placeHolograms() {

    }

    @Override
    protected FileConfiguration getConfigurationFile() {
        FileConfiguration basicSavedConfig = getBasicSavedConfig();
        final int[] point = {0};
        if (!basicSavedConfig.contains("points")) {
            basicSavedConfig.createSection("points");
        }
        ConfigurationSection points = basicSavedConfig.getConfigurationSection("points");
        area.forEach(new Consumer<BlockVector>() {
            @Override
            public void accept(BlockVector blockVector) {
                String prefix = "p-" + point[0];
                basicSavedConfig.set(prefix + ".x", blockVector.getBlockX());
                basicSavedConfig.set(prefix + ".y", blockVector.getBlockY());
                basicSavedConfig.set(prefix + ".z", blockVector.getBlockZ());
                point[0]++;
            }
        });
        return basicSavedConfig;
    }

    @Override
    public MineType getType() {
        return MineType.POLYHEDRAL;
    }

    @Override
    public boolean contains(double x, double y, double z) {
        return false;
    }

    @Override
    public Location getCenter() {
        return null;
    }


}
