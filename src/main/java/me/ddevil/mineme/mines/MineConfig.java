/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.mines;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.types.CircularMine;
import me.ddevil.mineme.mines.types.CuboidMine;
import me.ddevil.mineme.mines.types.PolygonMine;
import me.ddevil.shiroi.utils.PotionUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author DDevil_
 */
public class MineConfig {

    private final MineMe plugin;
    private final boolean allowChallengesOverrideHolograms;


    public static Mine loadMine(MineMe plugin, MineConfig c) {
        MineType type = c.getType();
        switch (type) {
            case CUBOID:
                return new CuboidMine(plugin, c);
            case CIRCULAR:
                return new CircularMine(plugin, c);
            case POLYGON:
                return new PolygonMine(plugin, c);
            default:
                return null;
        }
    }

    //General info
    private final boolean enabled;
    private final FileConfiguration config;
    private final World world;
    private final MineType type;
    private final String name;
    private final String alias;

    //Broadcast info
    private final String broadcastMessage;
    private final boolean broadcastOnReset;
    private final boolean broadcastNearby;
    private final double broadcastRadius;

    //Tecnical info
    private final int resetDelay;
    private final HashMap<ItemStack, Double> composition;
    private final boolean useCustomBroadcast;
    private final ArrayList<PotionEffect> potionEffects;
    private final boolean useEffects;
    //challenges
    private final boolean challengesEnabled;
    private final double challengeChance;
    private final List<String> challenges;

    public MineConfig(MineMe plugin, FileConfiguration mine) {
        //General
        this.config = mine;
        this.plugin = plugin;
        String worldname = mine.getString("world");
        this.world = Bukkit.getWorld(worldname);
        if (this.world == null) {
            throw new IllegalArgumentException("There isn't a world called '" + worldname + "'!");
        }
        this.enabled = mine.getBoolean("enabled");
        this.type = MineType.valueOf(mine.getString("type"));
        this.name = mine.getString("name");
        this.alias = mine.getString("alias");
        //Resets
        ConfigurationSection reset = config.getConfigurationSection("reset");
        resetDelay = reset.getInt("delay");
        //Broadcasts
        ConfigurationSection broadcast = reset.getConfigurationSection("broadcast");
        broadcastOnReset = broadcast.getBoolean("use");
        broadcastNearby = broadcast.getBoolean("nearbyOnly");
        broadcastRadius = mine.getDouble("nearbyRadius");
        //Custom emssages
        ConfigurationSection custom = broadcast.getConfigurationSection("custom");
        useCustomBroadcast = custom.getBoolean("useCustomBroadcast");
        broadcastMessage = custom.getString("customBroadcast");
        //Composition
        HashMap<ItemStack, Double> comp = new HashMap();
        if (!mine.contains("composition")) {
            mine.set("composition", comp);
        } else {
            for (String s : mine.getStringList("composition")) {
                String[] split = s.split("=");

                String[] materialanddata = split[0].split(":");
                Material mat = Material.valueOf(materialanddata[0]);
                Byte b = null;
                if (materialanddata.length > 1) {
                    try {
                        b = Byte.valueOf(materialanddata[1]);
                    } catch (NumberFormatException exception) {
                        plugin.debug(split[1] + " in " + s + "isn't a number! Setting byte to 0");
                        b = 0;
                    }
                } else {
                    b = 0;
                }
                ItemStack f = new ItemStack(mat, 1, (short) 0, b);
                try {
                    comp.put(f, Double.valueOf(split[1]));
                } catch (NumberFormatException e) {
                    plugin.debug(split[1] + " in " + s + "isn't a number!");
                }
            }
        }
        this.composition = comp;

        //Load effects
        this.potionEffects = new ArrayList();
        if (!config.contains("effects")) {
            config.set("effects.use", false);
            config.set("effects.list", potionEffects);
            useEffects = false;
        } else {
            ConfigurationSection effects = config.getConfigurationSection("effects");
            if (!effects.contains("list")) {
                effects.set("list", potionEffects);
            }
            this.useEffects = effects.getBoolean("use");
            List<String> stringList = effects.getStringList("list");
            for (String potionEffect : stringList) {
                try {
                    potionEffects.add(PotionUtils.parsePotionEffect(potionEffect));
                } catch (PotionUtils.PotionParseException ex) {
                    plugin.printException("There was an error parsing " + potionEffect + " from the effect list!", ex);
                }
            }
        }
        ConfigurationSection challenges = config.getConfigurationSection("challenges");
        this.challengesEnabled = challenges.getBoolean("enabled");
        this.challengeChance = challenges.getDouble("chance") / 100;
        this.challenges = challenges.getStringList("list");
        this.allowChallengesOverrideHolograms = challenges.getBoolean("overrideHolograms");
    }

    public boolean isUseCustomBroadcast() {
        return useCustomBroadcast;
    }

    public HashMap<ItemStack, Double> getComposition() {
        return composition;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getName() {
        return name;
    }

    public boolean isNearbyBroadcast() {
        return broadcastNearby;
    }

    public boolean isBroadcastOnReset() {
        return broadcastOnReset;
    }

    public int getResetDelay() {
        return resetDelay;
    }

    public double getBroadcastRadius() {
        return broadcastRadius;
    }

    public String getBroadcastMessage() {
        return broadcastMessage;
    }

    public World getWorld() {
        return world;
    }

    public MineType getType() {
        return type;
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public String getAlias() {
        return alias;
    }

    public List<PotionEffect> getEffects() {
        return potionEffects;
    }

    public boolean useEffects() {
        return useEffects;
    }

    public boolean isChallengesEnabled() {
        return challengesEnabled;
    }

    public double getChallengeChance() {
        return challengeChance;
    }

    public List<String> getChallenges() {
        return challenges;
    }

    public boolean allowChallengesOverrideHolograms() {
        return allowChallengesOverrideHolograms;
    }
}
