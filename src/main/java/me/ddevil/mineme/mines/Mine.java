/* 
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.mines;

import com.sk89q.worldedit.regions.Region;
import me.ddevil.mineme.challenges.Challenge;
import me.ddevil.mineme.challenges.ChallengeParameters;
import me.ddevil.mineme.challenges.ChallengeResultType;
import me.ddevil.mineme.regions.MineRegion;
import me.ddevil.mineme.task.MineStatsRecalculator;
import me.ddevil.mineme.utils.WorldEditIterator;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author DDevil_
 */
public interface Mine<R extends Region> extends Iterable<Block>, Listener {

    //<editor-fold desc="General" defaultstate="collapsed">

    /**
     * Get the mine's type enum.
     *
     * @return This mine's type
     * @see MineType
     */
    MineType getType();

    /**
     * The the mine's <b>name</b>, not alias.
     *
     * @return The mine's name
     */
    String getName();

    /**
     * Deletes the mine
     */
    void delete();

    void disable();

    /**
     * Resets the mine
     */
    void reset();

    /**
     * Save the mine to it's config file
     */
    void save();

    /**
     * Checks if this mine is deleted (Garbage Collector haven't finalized this
     * yet)
     *
     * @return true if this mine is deleted.
     */
    boolean isDeleted();


    /**
     * @param item The item to fill with (Must be placeable!).
     */
    void fill(ItemStack item);

    /**
     * Will remove all the blocks (not material) inside this mine, if
     * you wish to remove the material, use {@link #clearMaterials()} instead!
     */
    void clear();

    /**
     * Sets the mine's <b>alias</b>, not the name, one does not
     * simply change a mine's name, if you wish to change a mine's
     * name, you must create a new mine
     *
     * @param alias The alias to set
     */
    void setAlias(String alias);

    /**
     * Checks if the broken was already broken in the mine.
     *
     * @param block The block to check
     * @return true if the block was already broken.
     */
    boolean wasAlreadyBroken(Block block);

    /**
     * Get's a list of information about the mine
     *
     * @return The mine's technical info
     */
    List<String> getInfo();

    /**
     * @return true if {@link #getRemainingBlocks()} is less or equal to 0.
     */
    default boolean isCompletelyBroken() {
        return getRemainingBlocks() <= 0;
    }

    /**
     * Get's the mine's center
     *
     * @return The mine's center
     */
    Location getCenter();

    /**
     * Get the actual name that is displayed on broadcast messages, etc.
     *
     * @return The mine's alias.
     */
    String getAlias();

    /**
     * Get's the world the mine is in.
     *
     * @return The mine's world.
     */
    World getWorld();

    /**
     * Gets the minimum Y value
     *
     * @return the Y value
     */
    int getLowerY();

    /**
     * Gets the maximum Y value
     *
     * @return the Y value
     */
    int getUpperY();

    /**
     * Get's the time till next reset.
     *
     * @return The remaining time till the next reset.
     */
    int getTimeToNextReset();

    /**
     * @return The mine's reset delay in seconds
     */
    int getResetTime();

    /**
     * Check if the mine is enabled, this only work when you've deleted a mine
     * and haven't restarted the server yet, because mine's only get loaded if
     * enabled is set to true in it's config.
     *
     * @return true if it's enabled
     */
    boolean isEnabled();

    /**
     * @return The icon of this mine, the one that appears in the menu and on holograms if set to.
     */
    ItemStack getIcon();

    /**
     * Set's the enabled in the config
     *
     * @param enabled The boolean to set
     */
    void setEnabled(boolean enabled);

    /**
     * Sometimes math can go wrong, and we need to recalculate it, so what this does is, get every single player data, and see
     * how many blocks they have mined on the mine, this runs async.
     */
    MineStatsRecalculator recalculateStatistics();

    //</editor-fold>
    //<editor-fold desc="Broadcast" defaultstate="collapsed">
    boolean useCustomBroadcast();

    void setUseCustomBroadcast(boolean useCustomBroadcast);

    /**
     * Will
     *
     * @param delayInMinutes The delay to set in minutes
     */
    void setResetDelay(int delayInMinutes);

    /**
     * Return if the mine is set to broadcast it's reset message.
     *
     * @return true if mine is set to send message on reset, false otherwise
     */
    boolean isBroadcastOnReset();

    /**
     * Set's if the mine should broadcast it's reset message.
     *
     * @param broadcastOnReset boolean to set
     */
    void setBroadcastOnReset(boolean broadcastOnReset);

    /**
     * @param range The minimum range the player must be in to hear the broadcast message.
     */
    void setBroadcastRange(double range);

    /**
     * Check if the mine is set to broadcast a message on reset
     *
     * @return true if it is set
     */
    boolean broadcastOnReset();

    /**
     * Check if the mine is set to broadcast the reset message to nearby players
     * only
     *
     * @return true if it is set
     */
    boolean broadcastToNearbyOnly();

    /**
     * Get's the broadcast radius of the reset message.
     *
     * @return The broadcast eadius
     */
    double getBroadcastRadius();

    //</editor-fold>
    //<editor-fold desc="Composition" defaultstate="collapsed">

    /**
     * Get's a map of each material of the composition of the mine and it's
     * percentage
     *
     * @return The mine's composition
     */
    Map<ItemStack, Double> getComposition();

    /**
     * @return The total percentage of material in this mine
     * <br>
     * ex: A mine with 10% stone and 45% gold ore will return 55.0
     */
    double getTotalMaterialPercentage();

    /**
     * @return The material used in the mine's composition
     */
    List<ItemStack> getMaterials();

    /**
     * @return true if {@link #getTotalMaterialPercentage()} exceeds 100.0
     */
    default boolean isExceedingMaterials() {
        return getExceedingTotal() > 0;
    }

    /**
     * @return The exceeding percentage of material, will return 0 if isn't exceeding percentage
     */
    double getExceedingTotal();

    /**
     * Get's the material's percentage in the mines composition
     *
     * @param material The material to check
     * @return The percentage of the material in the mines composition, returns
     * 0 if the mine doesn't contains the material;
     */
    double getPercentage(ItemStack material);

    /**
     * Check if the mine contains said material.
     *
     * @param material The material to check
     * @return true if the mine contains this material in it's composition.
     */
    boolean containsMaterial(ItemStack material);

    /**
     * Delete's the old mine composition as set's the map as the new composition
     *
     * @param composition The composition map to set to the mine
     */
    void setComposition(Map<ItemStack, Double> composition);

    /**
     * Set's the material percentage in the mine's composition, override the
     * previous percentage if there was any, if the mine dind't contain the
     * material, adds it to the composition.
     *
     * @param material   The material to set
     * @param percentage The percentage to set
     */
    void setMaterialPercentage(ItemStack material, double percentage);

    /**
     * Add the material to the mine's composition with 0 percentage.
     * <br>
     * If the mine already contains the material, nothing will happen
     *
     * @param material The material to set
     */
    void addMaterial(ItemStack material);

    /**
     * Remove the material from the mine's composition, if the material is party
     * of the composition.
     *
     * @param material The material to remove
     */
    void removeMaterial(ItemStack material);

    /**
     * @return The total number of material
     * </br>
     * Ex: If a mine has in it's composition Stone and Gold Ore, this will return 2.
     */
    int getTotalMaterials();

    double getFreePercentage();

    void clearMaterials();

    ItemStack getRelativeItemStackInComposition(ItemStack item);

    boolean containsRelativeItemStackInComposition(ItemStack i);

    //</editor-fold>
    //<editor-fold desc="Area & Region" defaultstate="collapsed">
    MineRegion getRegion();

    R getWERegion();

    @Override
    default Iterator<Block> iterator() {
        return new WorldEditIterator(getWERegion());
    }

    /**
     * Check if the point on which all the 3 variables cross is inside the mine
     *
     * @param x X coordinate
     * @param y Y coordinate
     * @param z Z coordinate
     * @return true if is inside the mine
     */
    boolean contains(double x, double y, double z);

    /**
     * Check if the block is
     *
     * @param block The block to check.
     * @return true if it's inside, Oooh yeah baby
     */
    default boolean contains(Block block) {
        return contains(block.getLocation());
    }

    /**
     * Check if the location is inside this mine
     *
     * @param location The location to test
     * @return true if the location is inside the mine
     */
    default boolean contains(Location location) {
        return contains(location.getX(), location.getY(), location.getZ());
    }

    /**
     * Check if the player is inside this mine
     *
     * @param player The player to test
     * @return true is the player is inside the mine
     */
    default boolean contains(Player player) {
        return contains(player.getLocation());
    }

    /**
     * Get the mine's volume, duuuh
     *
     * @return the mine's volume
     */
    int getVolume();

    //</editor-fold>
    //<editor-fold desc="Internals (Don't touch this unless you are 100% sure of what you're doing!)" defaultstate="collapsed">

    /**
     * Remove's a second from the mines countdown, also updates the holograms,
     * if it contains any and holograms are set to do it
     */
    void secondCountdown();

    /**
     * Set the block as a broken block, so the mine contabilazes it in the
     * statistics
     *
     * @param block The block to set as broken
     */
    void setBlockAsBroken(Block block);

    //</editor-fold>
    //<editor-fold desc="Statistics" defaultstate="collapsed">
    long getLifetimeBrokenBlocks() throws Exception;

    long getLifetimeResets() throws Exception;

    long getTotalBlocksBroken(Player player);

    /**
     * Get the total number of blocks that haven't been broken yet since the
     * last reset
     *
     * @return The total remaining blocks
     */
    int getRemainingBlocks();

    /**
     * Get the total number of blocks that haven't been broken yet since the
     * last reset as a percentage
     *
     * @return The total remaining blocks as a percentage
     */
    float getPercentageRemaining();

    /**
     * Get the total number of blocks that have been broken since the last reset
     *
     * @return The total broken blocks
     */
    int getMinedBlocks();

    /**
     * Get the total number of blocks that have been broken yet since the last
     * reset as a percentage
     *
     * @return The total broken blocks as a percentage
     */
    float getPercentageMined();

    public Map<UUID, List<Block>> getPlayersLastBrokenBlocks();


    /**
     * @return All the players inside the mine at this moment.
     */
    default List<Player> getPlayersInside() {
        return getWorld().getPlayers().stream()
                .filter(this::contains)
                .collect(Collectors.toList());
    }

    /**
     * @return Almost the same as {@link #getCenter()}, but with the Y coordinates at {@link #getUpperY()}
     */
    default Location getTopCenterLocation() {
        Location center = getCenter().clone();
        center.setY(getUpperY());
        return center;
    }

    /**
     * Get's the Block/Second break speed
     *
     * @return The average number of blocks broken in the last second
     */
    int averageBreakSpeed();
    //</editor-fold>
    //<editor-fold desc="Challenges" defaultstate="collapsed">

    /**
     * @return True if a {@link me.ddevil.mineme.challenges.Challenge} is currently active on this mine.
     */
    default boolean isRunningAChallenge() {
        return getCurrentChallenge() != null;
    }

    /**
     * @return The challenges that is currently active on this mine, if there is none will return null
     * @see Challenge
     */
    Challenge getCurrentChallenge();

    /**
     * Will set and start the given challenges on this mine.
     * If there was another challenges running, it will be finished as a failure.
     *
     * @param challenge The challenges to set
     * @see ChallengeResultType
     */
    void forceSetCurrentChallenge(Challenge challenge);

    /**
     * Adds the given challenges to the mine's challenges queue. If
     * there is no challenges running at the time, the given challenges
     * will start immediately.
     *
     * @param challenge The challenges to be added
     */
    void addChallengeToQueue(Challenge challenge);

    boolean allowChallengesOverrideHolograms();

    void setAllowChallengesOverrideHolograms(boolean allowChallengesOverrideHolograms);

    //</editor-fold>
    //<editor-fold desc="Potions" defaultstate="collapsed">
    void addPotionEffect(PotionEffect effect);

    void removePotionEffect(PotionEffectType effectType);

    void clearEffects();

    boolean useEffects();

    PotionEffect getEffect(PotionEffectType type);

    List<PotionEffect> getEffects();

    List<ChallengeParameters> getChallengeParameters();

    boolean useChallenges();
    //</editor-fold>

}
