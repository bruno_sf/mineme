package me.ddevil.mineme.holograms;

/**
 * Created by BRUNO II on 05/07/2016.
 */
public enum HologramRefreshType {
    MINE_SYNC,
    CUSTOM_RATE
}
