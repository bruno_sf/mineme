/* 
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.messages;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.challenges.types.BasicChallenge;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.placeholders.MineMePlaceholder;
import me.ddevil.mineme.placeholders.PlaceholderRequest;
import me.ddevil.mineme.storage.StorageManager;
import me.ddevil.shiroi.message.internal.BaseMessageManager;
import me.ddevil.shiroi.misc.ColorDesign;
import me.ddevil.shiroi.utils.item.ItemBuilder;
import me.ddevil.shiroi.utils.item.ItemConversionException;
import me.ddevil.shiroi.utils.item.ItemUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MineMeMessageManager extends BaseMessageManager<MineMe> {

    private final FileConfiguration messagesConfig;
    private final StorageManager storageManager;
    private MineMeMessageManager instance;

    public MineMeMessageManager(MineMe plugin, ColorDesign colorDesign, String pluginPrefix, String messageSeparator, String header, char messageColorChar) {
        super(plugin, colorDesign, pluginPrefix, messageSeparator, header, messageColorChar);
        this.messagesConfig = plugin.getMessagesConfig();
        this.storageManager = plugin.getStorageManager();
    }

    //Mine Messages
    public String globalResetMessage;
    public String mineCreateMessage;

    //Error Messages
    public String noPermission;
    public String invalidArguments;

    public ItemStack createIcon(ConfigurationSection iconsection, Mine m) {
        return translateItemStack(ItemBuilder.createItem(iconsection, plugin).toItemStack(), m);
    }

    public ItemStack translateItemStack(ItemStack itemStack, Mine m) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta.hasDisplayName()) {
            itemMeta.setDisplayName(translateAll(itemMeta.getDisplayName(), m));
        }
        if (itemMeta.hasLore()) {
            itemMeta.setLore(translateAll(itemMeta.getLore(), m));
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    //Colors


    @Override
    public void setup() {

        super.setup();
        plugin.debug("Loading messages...");
        //Mine Messages
        globalResetMessage = translateColors(messagesConfig.getString("messages.mineReset"));
        mineCreateMessage = translateColors(messagesConfig.getString("messages.mineCreate"));
        //Error Messages
        noPermission = translateColors(messagesConfig.getString("messages.noPermission"));
        invalidArguments = translateColors(messagesConfig.getString("messages.invalidArguments"));
        plugin.debug("Messages loaded!");
    }

    public List<String> translateAll(List<String> get, Mine m) {
        ArrayList<String> strings = get.stream().map(s -> translateAll(s, m)).collect(Collectors.toCollection(ArrayList::new));
        return strings;
    }

    public static Pattern COMPOSITION_PATTERN = Pattern.compile("%composition:([\\w:]+)%", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    public String translateAll(String input, Mine m) {
        //Mine
        if (m != null) {
            PlaceholderRequest placeholderRequest = new PlaceholderRequest(null, m);
            for (MineMePlaceholder mineMePlaceholder : MineMePlaceholder.values()) {

                input.replace("{" + mineMePlaceholder.getReplacer() + "}", mineMePlaceholder.response(placeholderRequest));
            }
        } else {
            plugin.debug("The mine used to translate " + input + " is null! Skipping mine tags...", true);
        }
        Matcher ma = COMPOSITION_PATTERN.matcher(input);
        if (ma.find()) {
            String itemName = ma.group(1);
            ItemStack item = null;
            try {
                item = ItemUtils.convertFromInput(itemName);
                input = input.replace(ma.group(), String.valueOf(m.getPercentage(item)));
            } catch (ItemConversionException ex) {
                plugin.printException(input, ex);
            }
        }
        return translateAll(input);
    }

    public String getResetMessage(Mine m) {
        return translateAll(globalResetMessage, m);
    }

    @Override
    public String translateTags(String input) {
        if (pluginPrefix != null) {
            input = input.replace("{prefix}", pluginPrefix);
        }
        if (messageSeparator != null) {
            input = input.replace("{separator}", messageSeparator);
        }
        if (header != null) {
            input = input.replace("{header}", header);
        }
        return input;
    }

    public ItemStack createIcon(ConfigurationSection iconSection) {
        return translateItemStack(ItemBuilder.createItem(iconSection, plugin).toItemStack());
    }

    public ItemStack translateItemStack(ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta.hasDisplayName()) {
            itemMeta.setDisplayName(translateAll(itemMeta.getDisplayName()));
        }
        if (itemMeta.hasLore()) {
            itemMeta.setLore(translateAll(itemMeta.getLore()));
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public String translateAll(String input, BasicChallenge basicChallenge) {
        //Mine

        PlaceholderRequest placeholderRequest = new PlaceholderRequest(null, null, basicChallenge);
        for (MineMePlaceholder mineMePlaceholder : MineMePlaceholder.values()) {
            if (mineMePlaceholder.requireChallenge()) {
                input.replace("{" + mineMePlaceholder.response(placeholderRequest) + "}", mineMePlaceholder.response(placeholderRequest));
            }
        }
        return translateAll(input);
    }
}
