/* 
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import me.ddevil.mineme.challenges.ChallengeManager;
import me.ddevil.mineme.commands.MineMeCommand;
import me.ddevil.mineme.configuration.MineMeConfiguration;
import me.ddevil.mineme.conversion.MRLConverter;
import me.ddevil.mineme.economy.EconomyManager;
import me.ddevil.mineme.economy.VaultEconomyManager;
import me.ddevil.mineme.holograms.HologramAdapterType;
import me.ddevil.mineme.holograms.HologramManager;
import me.ddevil.mineme.holograms.HologramRefreshType;
import me.ddevil.mineme.holograms.managers.HolographicDisplaysManager;
import me.ddevil.mineme.messages.MineMeMessageManager;
import me.ddevil.mineme.mines.HologramCompatibleMine;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineConfig;
import me.ddevil.mineme.mines.MineManager;
import me.ddevil.mineme.placeholders.PlaceholderManager;
import me.ddevil.mineme.regions.MineRegionManager;
import me.ddevil.mineme.regions.RegionAdapterType;
import me.ddevil.mineme.regions.providers.WorldGuardMineRegionManager;
import me.ddevil.mineme.storage.StorageFactory;
import me.ddevil.mineme.storage.StorageManager;
import me.ddevil.mineme.storage.StorageManagerType;
import me.ddevil.mineme.ui.GUIManager;
import me.ddevil.mineme.ui.GUIResources;
import me.ddevil.shiroi.CustomPlugin;
import me.ddevil.shiroi.PluginSettings;
import me.ddevil.shiroi.command.ArrayCommandManager;
import me.ddevil.shiroi.command.CommandManager;
import me.ddevil.shiroi.config.ConstantsConfigurationManager;
import me.ddevil.shiroi.misc.ColorDesign;
import me.ddevil.shiroi.misc.DebugLevel;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;

@PluginSettings(loadConfig = true, commandClasses = {MineMeCommand.class})
public class MineMe extends CustomPlugin<MineMeMessageManager, ConstantsConfigurationManager<?>, ArrayCommandManager> {

    //<editor-fold desc="Configuration" defaultstate="collapsed">
    //Storage
    private File minesFolder;
    private File storageFolder;
    private File playerStorageFolder;
    private File mineStorageFolder;
    //World Edit
    private WorldEditPlugin WEP;
    //Scheduler ids
    private BukkitTask minesUpdater;
    private BukkitTask hologramUpdater;
    //Holograms
    private HologramManager hologramManager;
    private MineManager mineManager;
    private PlaceholderManager placeholderManager;
    private StorageManager storageManager;
    private GUIManager guiManager;
    private MineRegionManager regionManager;
    private ChallengeManager challengeManager;
    private EconomyManager economyManager;
    private FileConfiguration messagesConfig;
    private File messagesConfigFile;


    //</editor-fold>
    protected void initializeComponents() {
        //Get files references
        this.storageFolder = new File(getDataFolder().getPath(), "storage");
        this.minesFolder = new File(getDataFolder().getPath(), "mines");
        this.playerStorageFolder = new File(storageFolder, "players");
        this.mineStorageFolder = new File(storageFolder, "mines");
        //Load me.ddevil.config
        //Managers
        StorageManagerType type = StorageManagerType.valueOf(configManager.getValue(MineMeConfiguration.STORAGE_TYPE));
        debug("Loading Storage Manager (" + type + ") ...");
        storageManager = StorageFactory.fromType(type, this);
        if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
            debug("Found Vault! Loading Economy Manager...");
            this.economyManager = new VaultEconomyManager(this);
            this.economyManager.setup();
        }
        debug("Loading Mine Manager...");
        this.mineManager = new MineManager(this).setup();
        debug("Loading Command Manager...");
        debug("Loading Placeholder Manager...");
        this.placeholderManager = new PlaceholderManager(this).setup();
        debug("Loading Challenge Manager...");
        this.challengeManager = new ChallengeManager(this).setup();
    }

    @Override
    protected void doSetup() {
        WEP = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
        WorldEdit.getInstance().getEventBus().register(new me.ddevil.mineme.mines.MineManager.WorldEditManager(mineManager));
        if (configManager.getValue(MineMeConfiguration.CONVERT_FROM_MRL)) {
            debug("Converting MineResetLite...", true);
            new MRLConverter(this).start();
        }
        if (configManager.getValue(MineMeConfiguration.REGIONS_ENABLED)) {
            RegionAdapterType regionProvider = RegionAdapterType.valueOf(configManager.getValue(MineMeConfiguration.REGIONS_PROVIDER));
            switch (regionProvider) {
                case WORLD_GUARD:
                    if (Bukkit.getPluginManager().isPluginEnabled("WorldGuard")) {
                        regionManager = new WorldGuardMineRegionManager(this);
                    } else {
                        debug("WorldGuard was enabled in the me.ddevil.config, but is not installed in the server! Fixing...", true);
                        try {
                            pluginConfig.set("settings.region.enabled", false);
                            pluginConfig.save(new File(getDataFolder(), "config.yml"));
                        } catch (IOException ex) {
                            printException("There was a error fixing WorldGuard in the me.ddevil.config!", ex);
                        }
                    }
                    break;

            }
        }
        debug("Starting metrics...", DebugLevel.NO_BIG_DEAL);
        try {
            Metrics metrics = new Metrics(MineMe.this);
            metrics.start();
            debug("Metrics started!", DebugLevel.NO_BIG_DEAL);
        } catch (IOException ex) {
            printException("There was an error while trying to start metrics! Skipping", ex);
        }
        debug("Starting MineEditorGUI...", DebugLevel.NO_BIG_DEAL);
        GUIResources.setup(this);
        this.guiManager = new GUIManager(this).setup();
        debug("MineEditorGUI started!", DebugLevel.NO_BIG_DEAL);
        debug("Waiting the server before loading mines...", DebugLevel.NO_BIG_DEAL);
        Bukkit.getScheduler().runTaskLater(this, () -> {
            loadMines();
            debug("Starting timer...", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
            startTimers();
            debug("Timer started!", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
        }, 3l);
    }


    @Override
    public void onDisable() {
        unloadEverything();
        mineManager.saveAll();
    }

    public FileConfiguration getYAMLMineFile(Mine m) {
        return YamlConfiguration.loadConfiguration(getMineFile(m));
    }

    public File getMineFile(Mine m) {
        return new File(minesFolder.getPath(), m.getName() + ".yml");
    }


    @Override
    protected ColorDesign loadColorDesign() {
        ConfigurationSection value = configManager.getValue(MineMeConfiguration.COLOR_DESIGN);
        return ColorDesign.deserialize(value.getValues(true));
    }

    @Override
    protected MineMeMessageManager createMessageManager() {
        return new MineMeMessageManager(
                this,
                colorDesign,
                configManager.getValue(MineMeConfiguration.PLUGIN_PREFIX),
                configManager.getValue(MineMeConfiguration.MESSAGE_SEPARATOR),
                configManager.getValue(MineMeConfiguration.PLUGIN_HEADER),
                configManager.getValue(MineMeConfiguration.COLOR_CHAR).charAt(0)
        );
    }

    @Override
    protected ConstantsConfigurationManager<?> createConfigManager() {
        return new ConstantsConfigurationManager<>(this);
    }

    @Override
    protected ArrayCommandManager createCommandManager() {
        return new ArrayCommandManager<>(this);
    }


    @Override
    public void doReload() {
        debug("Stopping reseter task...");
        if (minesUpdater != null) {
            minesUpdater.cancel();

        }
        debug("Unloading...");
        unloadEverything();
        File pluginFolder = getDataFolder();
        if (!pluginFolder.exists()) {
            if (pluginSettings.loadConfig()) {
                debug("Plugin folder not found! Making one...", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                pluginFolder.mkdir();
            }
        }
        File pluginConfigFile = new File(pluginFolder, "config.yml");
        if (!pluginConfigFile.exists()) {
            if (pluginSettings.loadConfig()) {
                //Load from plugin
                debug("Config file not found! Making one...", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                loadResource(pluginConfigFile, "config.yml");
            }
        }
        File challengeConfigFile = new File(pluginFolder, "challenges.yml");
        if (!challengeConfigFile.exists()) {
            if (pluginSettings.loadConfig()) {
                //Load from plugin
                debug("Challenges me.ddevil.config file not found! Making one...", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                loadResource(challengeConfigFile, "challenges.yml");
            }
        }
        loadMines();
    }

    public void loadMines() {//setup mines
        debug("Loading mines", true);
        File[] mineFiles = minesFolder.listFiles();
        int i = 0;
        //Start loading mines
        for (File file : mineFiles) {
            String filename = file.getName();
            debug("-------------------------------", DebugLevel.NO_BIG_DEAL);

            debug("Attempting to setup " + filename + "...", DebugLevel.NO_BIG_DEAL);

            String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
            if (!"yml".equals(extension)) {
                debug(filename + " isn't a .yml file! This shouldn't be here! Skipping.", true);
                continue;
            }
            FileConfiguration mine = YamlConfiguration.loadConfiguration(file);
            //Get name
            String name = mine.getString("name");
            if (!mine.getBoolean("enabled")) {
                debug("Mine " + name + " is disabled, skipping.", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                continue;
            }
            //Load mine
            try {
                debug("Loading...");
                //Instanciate
                final MineConfig m = new MineConfig(this, mine);
                //Setup mines after bukkit is loaded
                Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                    @Override
                    public void run() {
                        mineManager.loadMine(m);
                    }
                }, 5l);

                debug("Loaded mine " + m.getName() + ".", true);
                i++;
            } catch (Throwable t) {
                printException("Something went wrong while loading " + file.getName() + " :( Are you sure you did everything right?", t);
            }
        }
        debug("Loaded " + i + " mines :D", true);
    }

    private void unloadEverything() {
        if (configManager.getValue(MineMeConfiguration.HOLOGRAMS_ENABLED)) {
            HologramsAPI.getHolograms(this).forEach(Hologram::delete);
        }
        mineManager.unregisterMines();
        minesUpdater = null;
    }

    public void startTimers() {
        if (minesUpdater != null) {
            minesUpdater.cancel();
            minesUpdater = null;
        }
        if (hologramUpdater != null) {
            hologramUpdater.cancel();
            hologramUpdater = null;
        }
        if (configManager.getValue(MineMeConfiguration.HOLOGRAMS_ENABLED)) {
            HologramAdapterType hologramType = HologramAdapterType.valueOf(configManager.getValue(MineMeConfiguration.HOLOGRAMS_PROVIDER));
            switch (hologramType) {
                case HOLOGRAPHIC_DISPLAYS:
                    if (Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
                        debug("Using Holographic Displays as the hologram adapter...", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                        //Set the hologram adapter
                        hologramManager = new HolographicDisplaysManager(this);
                        if (configManager.getValue(MineMeConfiguration.HOLOGRAMS_FORCE_DEFAULT_TEXT)) {
                            debug("Forcing all holograms to use default preset.", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                        }
                    } else {
                        //Enabled in me.ddevil.config, .jar is not in plugins
                        debug("HolographicDisplays was enabled in the me.ddevil.config, but is not installed in the server! Fixing...", true);
                        try {
                            pluginConfig.set("settings.holograms.enabled", false);
                            pluginConfig.save(new File(getDataFolder(), "config.yml"));
                        } catch (IOException ex) {
                            printException("There was a error fixing HolographicDisplays in the me.ddevil.config!", ex);
                        }
                    }
                    break;
            }
            //Start timer
            HologramRefreshType refreshType = HologramRefreshType.valueOf(configManager.getValue(MineMeConfiguration.HOLOGRAMS_UPDATE_TYPE));
            switch (refreshType) {
                case MINE_SYNC:
                    debug("Setting up hologram updater synchronized with mine reseter.", DebugLevel.NO_BIG_DEAL);
                    minesUpdater = new BukkitRunnable() {
                        @Override
                        public void run() {
                            for (Mine mine : mineManager.getMines()) {
                                if (mine instanceof HologramCompatibleMine) {
                                    HologramCompatibleMine hc = (HologramCompatibleMine) mine;
                                    hc.updateHolograms();
                                }
                                mine.secondCountdown();
                            }
                        }
                    }.runTaskTimer(this, 20l, 20l);
                    break;
                case CUSTOM_RATE:
                    minesUpdater = new BukkitRunnable() {
                        @Override
                        public void run() {
                            mineManager.getMines().forEach(Mine::secondCountdown);
                        }
                    }.runTaskTimerAsynchronously(this, 20l, 20l);
                    Integer refreshRate = configManager.getValue(MineMeConfiguration.HOLOGRAMS_CUSTOM_REFRESH_RATE);
                    debug("Setting up hologram updater @ " + refreshRate + " refresh speed.", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                    hologramUpdater = new BukkitRunnable() {
                        @Override
                        public void run() {
                            mineManager.getMines().stream().filter(mine -> mine instanceof HologramCompatibleMine).forEach(mine -> {
                                HologramCompatibleMine compatible = (HologramCompatibleMine) mine;
                                compatible.updateHolograms();
                            });
                        }
                    }.runTaskTimer(this, refreshRate.longValue(), refreshRate.longValue());
                    break;
            }
        } else {
            minesUpdater = new BukkitRunnable() {

                @Override
                public void run() {
                    mineManager.getMines().forEach(Mine::secondCountdown);
                }
            }.runTaskTimer(this, 20l, 20l);
        }
    }

    @Override
    protected void postInitialize() {
        messagesConfigFile = new File(getDataFolder(), "messages.yml");
        messagesConfig = YamlConfiguration.loadConfiguration(messagesConfigFile);
    }

    public File getStorageFolder() {
        return storageFolder;
    }

    public File getMinesFolder() {
        return minesFolder;
    }

    public HologramManager getHologramManager() {
        return hologramManager;
    }


    public MineManager getMineManager() {
        return mineManager;
    }

    public PlaceholderManager getPlaceholderManager() {
        return placeholderManager;
    }

    public StorageManager getStorageManager() {
        return storageManager;
    }

    public WorldEditPlugin getWorldEdit() {
        return WEP;
    }

    public GUIManager getGUIManager() {
        return guiManager;
    }

    public MineRegionManager getRegionManager() {
        return regionManager;
    }

    public ChallengeManager getChallengeManager() {
        return challengeManager;
    }

    public EconomyManager getEconomyManager() {
        return economyManager;
    }

    public File getPlayerStorageFolder() {
        return playerStorageFolder;
    }

    public File getMineStorageFolder() {
        return mineStorageFolder;
    }

    public GUIManager getGuiManager() {
        return guiManager;
    }


    public FileConfiguration getMessagesConfig() {
        return messagesConfig;
    }
}
