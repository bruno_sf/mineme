/*
 * Copyright (C) 2016 DDevil_
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ddevil.mineme.conversion;

import com.koletar.jj.mineresetlite.MineResetLite;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineManager;
import me.ddevil.mineme.mines.types.CuboidMine;
import me.ddevil.shiroi.task.CustomThread;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author DDevil_
 */
public class MRLConverter extends CustomThread {

    private final MineManager mineManager;
    private final MineMe plugin;
    private final File pluginFolder;

    public MRLConverter(MineMe plugin) {
        this.plugin = plugin;
        this.pluginFolder = plugin.getDataFolder();
        this.mineManager = plugin.getMineManager();
    }


    @Override
    public void doRun() {
        ArrayList<Mine> mines = new ArrayList<>();
        int i = 0;
        for (com.koletar.jj.mineresetlite.Mine m : MineResetLite.instance.mines) {
            String name = m.getName();
            plugin.debug("Converting mine " + name + "...", true);
            CuboidMine cm;
            World world = m.getWorld();
            Location pos1 = new Location(world, m.getMinX(), m.getMinY(), m.getMinZ());
            Location pos2 = new Location(world, m.getMaxX(), m.getMaxY(), m.getMaxZ());
            cm = new CuboidMine(plugin,
                    name,
                    pos1,
                    pos2);
            mineManager.registerMine(cm);
            mines.add(cm);
            i++;
        }
        try {
            plugin.getConfigurationManager().convertedMineResetLite();
        } catch (IOException e) {
            plugin.printException("There was a problem saving the plugin me.ddevil.config after converting MRL!", e);
        }
        plugin.debug(i + " mines converted!", true);
        plugin.debug("Disabling MineResetLite to avoid incompatibilities!", true);
        plugin.debug("Remember to remove MineResetLite from the plugins folder to prevent incompatibilities!", true);
        Bukkit.getPluginManager().disablePlugin(MineResetLite.instance);
        plugin.debug("MineResetLite disabled!", true);

    }
}
