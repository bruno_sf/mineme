package me.ddevil.mineme.economy;

import me.ddevil.mineme.MineMe;
import me.ddevil.shiroi.misc.BukkitToggleable;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;

/**
 * Created by BRUNO II on 10/08/2016.
 */
public class VaultEconomyManager extends BukkitToggleable<MineMe> implements EconomyManager {
    private final Economy economy;

    public VaultEconomyManager(MineMe plugin) {
        super(plugin);
        economy = plugin.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
    }

    @Override
    public void giveMoney(Player p, long quantity) {
        economy.depositPlayer(p, quantity);
    }
}
