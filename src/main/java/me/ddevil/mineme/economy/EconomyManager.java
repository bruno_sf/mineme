package me.ddevil.mineme.economy;

import me.ddevil.shiroi.utils.misc.Toggleable;
import org.bukkit.entity.Player;

/**
 * Created by BRUNO II on 10/08/2016.
 */
public interface EconomyManager extends Toggleable{
    void giveMoney(Player p, long quantity);
}
