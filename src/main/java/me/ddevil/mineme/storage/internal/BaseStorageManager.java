package me.ddevil.mineme.storage.internal;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.storage.StorageRequest;
import me.ddevil.mineme.storage.dao.MineInfo;
import me.ddevil.mineme.storage.dao.PlayerInfo;
import me.ddevil.mineme.storage.StorageManager;
import me.ddevil.shiroi.misc.BukkitToggleable;

import java.util.Map;
import java.util.UUID;

/**
 * Created by bruno on 15/09/2016.
 */
public abstract class BaseStorageManager extends BukkitToggleable<MineMe> implements StorageManager {

    private final LoadingCache<String, MineInfo> mineDataCache;
    private final LoadingCache<UUID, PlayerInfo> playerDataCache;

    public BaseStorageManager(MineMe plugin) {
        super(plugin);
        playerDataCache = CacheBuilder.newBuilder().build(createPlayerCacheLoader());
        mineDataCache = CacheBuilder.newBuilder().build(createMineCacheLoader());
    }

    protected abstract CacheLoader<UUID, PlayerInfo> createPlayerCacheLoader();

    protected abstract CacheLoader<String, MineInfo> createMineCacheLoader();

    public LoadingCache<UUID, PlayerInfo> getPlayerDataCache() {
        return playerDataCache;
    }

    @Override
    public PlayerInfo getPlayerInfo(UUID uuid) throws Exception {
        return playerDataCache.get(uuid);
    }

    @Override
    public MineInfo getMineInfo(String mineName) throws Exception {
        return mineDataCache.get(mineName);
    }

    @Override
    public void addReset(StorageRequest m) throws Exception {
        Map<UUID, Integer> blocksMap = m.getBlocksMap();
        blocksMap.entrySet().forEach(uuidLongEntry -> {
            try {
                UUID key = uuidLongEntry.getKey();
                PlayerInfo playerInfo = getPlayerInfo(key);
                playerInfo.addBlocks(m.getMine(), uuidLongEntry.getValue());
                savePlayer(playerInfo);
                playerDataCache.refresh(key);
            } catch (Exception e) {
                return;
            }
        });
        MineInfo mineInfo = getMineInfo(m.getMine());
        mineInfo.addBlocks(m.getBlocksToAdd());
        saveMine(mineInfo);
    }


}
