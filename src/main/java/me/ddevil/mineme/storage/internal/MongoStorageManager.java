package me.ddevil.mineme.storage.internal;

import com.google.common.cache.CacheLoader;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.configuration.MineMeConfiguration;
import me.ddevil.mineme.storage.dao.MineInfo;
import me.ddevil.mineme.storage.dao.PlayerInfo;
import org.bson.Document;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by BRUNO II on 10/08/2016.
 */
public class MongoStorageManager extends BaseStorageManager {
    private final MongoClient client;
    private final int port;
    private final String host;
    private final String databaseName;
    private final MongoDatabase database;
    private final MongoCollection<Document> playerCollection;
    private final MongoCollection<Document> minesCollection;

    public MongoStorageManager(MineMe plugin) {
        super(plugin);
        MineMeConfigurationManager configManager = plugin.getConfigManager();
        this.port = configManager.getValue(MineMeConfiguration.STORAGE_PORT);
        this.host = configManager.getValue(MineMeConfiguration.STORAGE_HOST);
        this.databaseName = configManager.getValue(MineMeConfiguration.STORAGE_DATABASE);
        if (configManager.getValue(MineMeConfiguration.STORAGE_USE_CREDENTIALS)) {
            client = new MongoClient(
                    new ServerAddress(host, port),
                    new ImmutableList.Builder<MongoCredential>().add(
                            MongoCredential.createCredential(
                                    configManager.getValue(MineMeConfiguration.STORAGE_USERNAME),
                                    databaseName,
                                    configManager.getValue(MineMeConfiguration.STORAGE_PASSWORD).toCharArray()
                            )
                    ).build()
            );
        } else {
            client = new MongoClient(host, port);
        }
        this.database = client.getDatabase(databaseName);
        this.playerCollection = database.getCollection(configManager.getValue(MineMeConfiguration.STORAGE_PLAYERS_COLLECTION));
        this.minesCollection = database.getCollection(configManager.getValue(MineMeConfiguration.STORAGE_MINES_COLLECTION));
    }

    @Override
    protected CacheLoader<UUID, PlayerInfo> createPlayerCacheLoader() {
        return new CacheLoader<UUID, PlayerInfo>() {
            @Override
            public PlayerInfo load(UUID uuid) throws Exception {
                Document playerDocument = playerCollection.find(new Document("_id", uuid)).first();
                PlayerInfo playerInfo;
                if (playerDocument != null) {
                    playerInfo = PlayerInfo.deserialize(playerDocument);
                } else {
                    playerInfo = PlayerInfo.createNew(uuid);
                    savePlayer(playerInfo);
                }
                return playerInfo;
            }
        };
    }

    @Override
    protected CacheLoader<String, MineInfo> createMineCacheLoader() {
        return new CacheLoader<String, MineInfo>() {
            @Override
            public MineInfo load(String s) throws Exception {
                Document id = minesCollection.find(new Document("_id", s)).first();
                if (id != null) {
                    return MineInfo.deserialize(id);
                } else {
                    return MineInfo.createNew(s);
                }
            }
        };
    }

    @Override
    public List<PlayerInfo> getEveryPlayerInfo() throws Exception {
        return Lists.newArrayList(playerCollection.find()).stream().map(PlayerInfo::deserialize).collect(Collectors.toList());
    }

    @Override
    public void saveMine(MineInfo info) throws Exception {

    }

    @Override
    public void savePlayer(PlayerInfo info) throws Exception {
        playerCollection.findOneAndReplace(new Document("_id", info.getUUID()), new Document(info.serialize()), new FindOneAndReplaceOptions().upsert(true));
    }
}
