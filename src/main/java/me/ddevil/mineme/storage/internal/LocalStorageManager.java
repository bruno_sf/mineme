package me.ddevil.mineme.storage.internal;

import com.google.common.cache.CacheLoader;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.storage.dao.MineInfo;
import me.ddevil.mineme.storage.dao.MineMeInfo;
import me.ddevil.mineme.storage.dao.PlayerInfo;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by bruno on 15/09/2016.
 */
public class LocalStorageManager extends BaseStorageManager {
    private final File playerStorageFolder;
    private final File mineStorageFolder;

    public LocalStorageManager(MineMe plugin) {
        super(plugin);
        this.playerStorageFolder = plugin.getPlayerStorageFolder();
        this.mineStorageFolder = plugin.getMineStorageFolder();
        if (!playerStorageFolder.exists()) {
            playerStorageFolder.mkdir();
        }
        if (!mineStorageFolder.exists()) {
            mineStorageFolder.mkdir();
        }
    }

    @Override
    protected CacheLoader<UUID, PlayerInfo> createPlayerCacheLoader() {
        return new CacheLoader<UUID, PlayerInfo>() {
            @Override
            public PlayerInfo load(UUID uuid) throws Exception {
                File file = getPlayerFile(uuid);
                PlayerInfo playerInfo;
                if (file.exists()) {
                    JSONObject piJson = decodeFromBase64(file);
                    playerInfo = PlayerInfo.deserialize(piJson);
                } else {
                    playerInfo = PlayerInfo.createNew(uuid);
                    savePlayer(playerInfo);
                }
                return playerInfo;
            }
        };
    }

    @Override
    protected CacheLoader<String, MineInfo> createMineCacheLoader() {
        return new CacheLoader<String, MineInfo>() {
            @Override
            public MineInfo load(String s) throws Exception {
                File mineFile = getMineFile(s);
                MineInfo mineInfo;
                if (mineFile.exists()) {
                    JSONObject jsonObject = decodeFromBase64(mineFile);
                    mineInfo = MineInfo.deserialize(jsonObject);
                } else {
                    mineInfo = MineInfo.createNew(s);
                }
                return mineInfo;
            }
        };
    }

    public JSONObject decodeFromBase64(File file) throws Exception {
        FileReader fileReader = new FileReader(file);
        String base64String = new String();
        while (fileReader.ready()) {
            base64String += (char) fileReader.read();
        }
        fileReader.close();
        return (JSONObject) new JSONParser().parse(new String(Base64.decodeBase64(base64String)));
    }

    public File getMineFile(String mineName) {
        return new File(mineStorageFolder, mineName + MineInfo.MINE_INFO_EXTENSION);
    }

    public File getPlayerFile(UUID uuid) {
        return new File(playerStorageFolder, uuid.toString() + PlayerInfo.PLAYER_INFO_EXTENSION);
    }


    @Override
    public void saveMine(MineInfo info) throws Exception {
        saveInfo(info, getMineFile(info.getMineName()));
    }

    public void saveInfo(MineMeInfo info, File file) throws Exception {
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(new String(Base64.encodeBase64(info.serialize().toJSONString().getBytes())));
        fileWriter.close();
    }

    @Override
    public void savePlayer(PlayerInfo info) throws Exception {
        saveInfo(info, getPlayerFile(info.getUUID()));
    }

    @Override
    public List<PlayerInfo> getEveryPlayerInfo() throws Exception {
        List<PlayerInfo> list = new ArrayList<>();
        for (File file : playerStorageFolder.listFiles()) {
            list.add(PlayerInfo.deserialize(decodeFromBase64(file)));
        }
        return list;
    }
}
