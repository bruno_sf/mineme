package me.ddevil.mineme.storage.internal;

import com.google.common.cache.CacheLoader;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.storage.dao.MineInfo;
import me.ddevil.mineme.storage.dao.PlayerInfo;

import java.util.List;
import java.util.UUID;

/**
 * Created by bruno on 28/09/2016.
 *
 * TODO implement
 */
public class MySQLStorageManager extends BaseStorageManager {
    public MySQLStorageManager(MineMe plugin) {
        super(plugin);
    }

    @Override
    public List<PlayerInfo> getEveryPlayerInfo() throws Exception {
        return null;
    }

    @Override
    public void saveMine(MineInfo info) throws Exception {

    }

    @Override
    public void savePlayer(PlayerInfo info) throws Exception {

    }

    @Override
    protected CacheLoader<UUID, PlayerInfo> createPlayerCacheLoader() {
        return null;
    }

    @Override
    protected CacheLoader<String, MineInfo> createMineCacheLoader() {
        return null;
    }
}
