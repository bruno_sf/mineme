package me.ddevil.mineme.storage.dao;

import com.google.common.collect.ImmutableMap;
import me.ddevil.mineme.mines.Mine;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by bruno on 15/09/2016.
 */
public class PlayerInfo implements MineMeInfo {
    /**
     * mmpd = MineMePlayerData
     *
     * @see MineInfo
     */
    public static final String PLAYER_INFO_EXTENSION = ".mmpd";
    private final UUID uuid;
    private final Map<String, Integer> brokenBlocks;

    /**
     * This is for internal use only, but I can't control what you want to do :P
     *
     * @param uuid
     * @param brokenBlocks
     */
    private PlayerInfo(UUID uuid, Map<String, Integer> brokenBlocks) {
        this.uuid = uuid;
        this.brokenBlocks = brokenBlocks;
    }

    public static PlayerInfo deserialize(Map<String, Object> map) {
        UUID uuid = UUID.fromString((String) map.get("_id"));
        Map<String, Integer> mineBreakMap = (Map<String, Integer>) map.get("brokenBlocks");
        return new PlayerInfo(uuid, mineBreakMap);
    }

    public void addBlocks(Mine<?> mine, int blocks) {
        String name = mine.getName();
        int total;
        if (brokenBlocks.containsValue(name)) {
            total = brokenBlocks.get(name);
        } else {
            total = 0;
        }
        brokenBlocks.put(name, total + blocks);
    }

    public UUID getUUID() {
        return uuid;
    }

    public Map<String, Integer> getBrokenBlocks() {
        return brokenBlocks;
    }


    public static PlayerInfo createNew(UUID uuid) {
        return new PlayerInfo(uuid, new HashMap());
    }


    @Override
    public JSONObject serialize() {
        return new JSONObject(new ImmutableMap.Builder<String, Object>()
                .put("brokenBlocks", brokenBlocks)
                .put("_id", uuid.toString())
                .build());
    }

    public int getBrokenBlocks(Mine<?> mine) {
        return getBrokenBlocks(mine.getName());
    }

    public int getBrokenBlocks(String name) {
        return brokenBlocks.get(name);
    }
}
