package me.ddevil.mineme.storage.dao;


import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.json.simple.JSONObject;

/**
 * Created by bruno on 15/09/2016.
 */
public interface MineMeInfo extends ConfigurationSerializable {
    @Override
    JSONObject serialize();

}
