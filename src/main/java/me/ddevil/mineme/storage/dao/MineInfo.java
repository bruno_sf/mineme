package me.ddevil.mineme.storage.dao;

import com.google.common.collect.ImmutableMap;
import org.json.simple.JSONObject;

import java.util.Map;

/**
 * Created by bruno on 15/09/2016.
 */
public class MineInfo implements MineMeInfo {
    /**
     * mmmd = MineMeMineData
     *
     * @see PlayerInfo
     */
    public static final String MINE_INFO_EXTENSION = ".mmmd";
    private static final String NAME_IDENTIFIER = "_id";
    private static final String BROKEN_BLOCKS_IDENTIFIER = "totalBrokenBlocks";
    private static final String RESETS_IDENTIFIER = "totalResets";
    private final String mineName;
    private long totalBrokenBlocks;
    private long totalResets;

    private MineInfo(String mineName) {
        this.mineName = mineName;
    }

    private MineInfo(String mineName, long totalBrokenBlocks, long totalResets) {
        this.mineName = mineName;
        this.totalBrokenBlocks = totalBrokenBlocks;
        this.totalResets = totalResets;
    }

    public String getMineName() {
        return mineName;
    }

    @Override
    public JSONObject serialize() {
        return new JSONObject(new ImmutableMap.Builder<String, Object>()
                .put(NAME_IDENTIFIER, mineName)
                .put(BROKEN_BLOCKS_IDENTIFIER, totalBrokenBlocks)
                .put("totalResets", totalResets)
                .build());
    }

    public long getTotalBrokenBlocks() {
        return totalBrokenBlocks;
    }

    public long getTotalResets() {
        return 0;
    }

    public static MineInfo createNew(String s) {
        return new MineInfo(s);
    }

    public static MineInfo deserialize(Map<String, Object> map) {
        String name = (String) map.get(NAME_IDENTIFIER);
        long totalBrokenBlocks = (long) map.get(BROKEN_BLOCKS_IDENTIFIER);
        long totalResets = (long) map.get(RESETS_IDENTIFIER);
        return new MineInfo(name, totalBrokenBlocks, totalResets);
    }

    public void addBlocks(long blocksToAdd) {
        addBlocks(blocksToAdd, true);
    }

    public void addBlocks(long blocksToAdd, boolean reset) {
        totalBrokenBlocks += blocksToAdd;
        if (reset) {
            totalResets++;
        }
    }

    public void setBlocks(long blocks) {
        totalBrokenBlocks = blocks;
    }
}
