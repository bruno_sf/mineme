package me.ddevil.mineme.storage;

import me.ddevil.mineme.mines.Mine;
import org.bukkit.block.Block;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by BRUNO II on 16/07/2016.
 */
public final class StorageRequest {
    private final Mine<?> mine;
    private final long blocksToAdd;
    private final Map<UUID, Integer> blocksMap;

    public StorageRequest(Mine<?> mine) {
        this.mine = mine;
        blocksToAdd = mine.getMinedBlocks();
        Map<UUID, List<Block>> playersLastBrokenBlocks = mine.getPlayersLastBrokenBlocks();
        blocksMap = new HashMap();
        for (UUID uuid : playersLastBrokenBlocks.keySet()) {
            blocksMap.put(uuid, playersLastBrokenBlocks.get(uuid).size());
        }
    }

    public Mine<?> getMine() {
        return mine;
    }

    public long getBlocksToAdd() {
        return blocksToAdd;
    }

    public Map<UUID, Integer> getBlocksMap() {
        return blocksMap;
    }
}
