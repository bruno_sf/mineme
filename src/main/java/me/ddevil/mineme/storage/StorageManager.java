package me.ddevil.mineme.storage;

import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.storage.dao.MineInfo;
import me.ddevil.mineme.storage.dao.MineMeInfo;
import me.ddevil.mineme.storage.dao.PlayerInfo;
import me.ddevil.shiroi.utils.misc.Toggleable;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * Created by BRUNO II on 16/07/2016.
 */
public interface StorageManager extends Toggleable {


    MineInfo getMineInfo(String mineName) throws Exception;

    default MineInfo getMineInfo(Mine<?> mine) throws Exception {
        return getMineInfo(mine.getName());
    }

    PlayerInfo getPlayerInfo(UUID uuid) throws Exception;

    List<PlayerInfo> getEveryPlayerInfo() throws Exception;

    default PlayerInfo getPlayerInfo(Player player) throws Exception {
        return getPlayerInfo(player.getUniqueId());
    }

    void addReset(StorageRequest m) throws Exception;

    default void save(MineMeInfo info) throws Exception {
        if (info instanceof PlayerInfo) {
            savePlayer(((PlayerInfo) info));
        } else if (info instanceof MineInfo) {
            saveMine(((MineInfo) info));
        } else {
            throw new IllegalArgumentException("There is no way to save " + info.getClass().getName() + "!");
        }
    }

    void saveMine(MineInfo info) throws Exception;

    void savePlayer(PlayerInfo info) throws Exception;

}
