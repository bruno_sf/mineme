package me.ddevil.mineme.storage;

/**
 * Created by BRUNO II on 16/07/2016.
 */
public enum StorageManagerType {
    LOCAL,
    MYSQL,
    MONGO,
    SQLITE;
}
