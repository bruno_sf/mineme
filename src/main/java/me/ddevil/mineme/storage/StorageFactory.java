package me.ddevil.mineme.storage;

import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.storage.internal.LocalStorageManager;
import me.ddevil.mineme.storage.internal.MongoStorageManager;
import me.ddevil.mineme.storage.internal.MySQLStorageManager;

/**
 * Created by bruno on 28/09/2016.
 */
public class StorageFactory {
    public static StorageManager fromType(StorageManagerType type, MineMe plugin) {
        switch (type) {
            case MONGO:
                return new MongoStorageManager(plugin);
            case MYSQL:
                return new MySQLStorageManager(plugin);
            case SQLITE:
                //TODO SQLite storage Manager
            default:
                return new LocalStorageManager(plugin);
        }
    }
}
