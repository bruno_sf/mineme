package me.ddevil.mineme.task;

import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.storage.StorageManager;
import me.ddevil.mineme.storage.dao.MineInfo;
import me.ddevil.mineme.storage.dao.PlayerInfo;
import me.ddevil.shiroi.task.CustomThread;

import java.util.List;
import java.util.function.Consumer;

/**
 * Created by bruno on 15/09/2016.
 */
public class MineStatsRecalculator extends CustomThread {
    private final Mine<?> mine;
    private final StorageManager storageManager;
    private MineInfo mineInfo;

    public MineStatsRecalculator(Mine<?> mine, StorageManager storageManager) {
        this.mine = mine;
        this.storageManager = storageManager;
    }

    private long totalBrokenBlocks = 0l;

    @Override
    public void doRun() {
        try {
            List<PlayerInfo> everyPlayerInfo = storageManager.getEveryPlayerInfo();
            everyPlayerInfo.forEach(new Consumer<PlayerInfo>() {
                @Override
                public void accept(PlayerInfo playerInfo) {
                    totalBrokenBlocks += playerInfo.getBrokenBlocks(mine);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.mineInfo = storageManager.getMineInfo(mine);
            mineInfo.setBlocks(totalBrokenBlocks);
            storageManager.saveMine(mineInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Mine<?> getMine() {
        return mine;
    }

    public StorageManager getStorageManager() {
        return storageManager;
    }

    public long getTotalBrokenBlocks() {
        return totalBrokenBlocks;
    }

    public MineInfo getMineInfo() {
        return mineInfo;
    }
}
