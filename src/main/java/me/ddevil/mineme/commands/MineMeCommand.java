package me.ddevil.mineme.commands;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.CylinderSelection;
import com.sk89q.worldedit.bukkit.selections.Polygonal2DSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import me.ddevil.mineme.MineMe;
import me.ddevil.mineme.exception.UnsupportedWorldEditRegionType;
import me.ddevil.mineme.messages.MineMeMessageManager;
import me.ddevil.mineme.mines.HologramCompatibleMine;
import me.ddevil.mineme.mines.Mine;
import me.ddevil.mineme.mines.MineManager;
import me.ddevil.mineme.mines.types.CircularMine;
import me.ddevil.mineme.mines.types.CuboidMine;
import me.ddevil.mineme.mines.types.PolygonMine;
import me.ddevil.shiroi.commands.AutoRegisterCommand;
import me.ddevil.shiroi.commands.PluginCommand;
import me.ddevil.shiroi.commands.framework.Command;
import me.ddevil.shiroi.commands.framework.CommandArgs;
import me.ddevil.shiroi.utils.MessageColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by BRUNO II on 29/06/2016.
 */
@AutoRegisterCommand
public class MineMeCommand extends PluginCommand<MineMe> {

    private final MineManager mineManager;
    private final MineMeMessageManager messageManager;

    public MineMeCommand(MineMe plugin) {
        super(plugin);
        this.mineManager = plugin.getMineManager();
        this.messageManager = plugin.getMessageManager();
    }

    @Command(name = "mineme", aliases = {"mm", "mrl", "mine", "mines"}, description = "Main MineMe command, use to access MineMe functions", permission = "mineme.admin", usage = "/mineme (option)")
    public void main(CommandArgs args) {
        plugin.getMessageManager().sendMessage(args.getSender(),
                new String[]{
                        "&r",
                        "%header%",
                        "$2Others cool aliases: %1mrl, mm, mine, mines",
                        MessageColor.ERROR + " () = Obligatory " + MessageColor.PRIMARY + "/" + MessageColor.ERROR + " [] = optional",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "create (name) [broadcast message] [nearbyBroadcast] [getBroadcastRadius] " + MessageColor.NEUTRAL + "Creates a new mine full of stone :D",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "delete (name) " + MessageColor.NEUTRAL + "Deletes the specified mine",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "info (name) " + MessageColor.NEUTRAL + "Displays infos about the specified mine",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "list " + MessageColor.NEUTRAL + "List all the loaded mines.",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "reset (name|all) " + MessageColor.NEUTRAL + "Reset the mine. (If you put all as the name, will reset all the mines)",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "help " + MessageColor.NEUTRAL + "Shows this.",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "gui " + MessageColor.NEUTRAL + "Open's MEGUI (MineEditorGUI).",
                        MessageColor.PRIMARY + "/mineme " + MessageColor.SECONDARY + "reload " + MessageColor.NEUTRAL + "Reloads the me.ddevil.config. :)",
                        MessageColor.ERROR + "NEVER USE /RELOAD (Sincerely, every Minecraft Developer ever)",
                        "%header%"}
        );
    }

    @Command(name = "mineme.create", aliases = {"mm.create", "mrl.create", "mine.create", "mines.create", "mineme.add", "mm.add", "mrl.add", "mine.add", "mines.add"}, description = "Create a new mine", permission = "mineme.admin", inGameOnly = true)
    public void create(CommandArgs args) {
        Player p = args.getPlayer();
        if (args.length() > 0) {
            String mineName = args.getArgs(0);
            if (!mineManager.isNameAvailable(mineName)) {
                messageManager.sendMessage(p, "There is already a mine named " + MessageColor.PRIMARY + mineName + MessageColor.NEUTRAL + "! Please select another one.");
                return;
            }
            Selection sel = plugin.getWorldEdit().getSelection((Player) p);
            if (sel == null) {
                messageManager.sendMessage(p, "You don't have a selection in WorldEdit! Type //wand and select 2 points");
                return;
            }
            Location loc1 = sel.getMaximumPoint();
            Location loc2 = sel.getMinimumPoint();
            if (loc1 == null) {
                messageManager.sendMessage(p, "You don't have a position 1 selected in WorldEdit!");
                return;
            }
            if (loc2 == null) {
                messageManager.sendMessage(p, "You don't have a position 2 selected in WorldEdit!");
                return;
            }
            int delay;
            if (args.length() > 1) {
                delay = Integer.valueOf(args.getArgs(1));
            } else {
                delay = 5;
            }
            boolean broadcast;
            if (args.length() > 2) {
                broadcast = Boolean.valueOf(args.getArgs(2));
            } else {
                broadcast = true;
            }
            boolean nearby;
            if (args.length() > 3) {
                nearby = Boolean.valueOf(args.getArgs(3));
            } else {
                nearby = true;
            }
            double radius;
            if (args.length() > 4) {
                try {
                    radius = Double.valueOf(args.getArgs(4));
                } catch (Exception e) {
                    radius = 50;
                }
            } else {
                radius = 50;
            }
            createNewMine((Player) p, mineName, sel);
        } else {
            messageManager.sendMessage(p, new String[]{
                    "You need to specify a name!"
            });
        }
    }

    @Command(name = "mineme.delete", aliases = {"mm.delete", "mrl.delete", "mine.delete", "mines.delete", "mineme.remove", "mm.remove", "mrl.remove", "mine.remove", "mines.remove"}, description = "Deletes a mine", permission = "mineme.admin")
    public void delete(CommandArgs args) {
        CommandSender p = args.getSender();
        if (args.length() > 0) {
            String name = args.getArgs(0);
            Mine m = mineManager.getMine(name);
            if (m != null) {
                m.delete();
                messageManager.sendMessage(p, "Mine " + MessageColor.PRIMARY + m.getName() + MessageColor.NEUTRAL + " was deleted! :D");
            } else {
                messageManager.sendMessage(p, "Could not find mine " + name + ".");
                listMines(p);
            }
        } else {
            messageManager.sendMessage(p, new String[]{
                    "You need to specify a name!"
            });
        }
    }

    @Command(name = "mineme.reset", aliases = {"mm.reset", "mrl.reset", "mine.reset", "mines.reset"}, description = "Reset a Mine", permission = "mineme.admin")
    public void reset(CommandArgs args) {
        CommandSender p = args.getSender();
        if (args.length() > 0) {
            String name = args.getArgs(0);
            if (name.equalsIgnoreCase("all")) {
                for (Mine mb : mineManager.getMines()) {
                    mb.reset();
                }
                messageManager.sendMessage(p, "Reseted all mines!");
                return;

            }
            Mine m = mineManager.getMine(name);
            if (m != null) {
                m.reset();
                messageManager.sendMessage(p, "Mine " + MessageColor.PRIMARY + m.getName() + MessageColor.NEUTRAL + " was reseted! :D");
            } else {
                messageManager.sendMessage(p, "Could not find mine " + name + ".");
                listMines(p);
            }
        } else {
            messageManager.sendMessage(p, new String[]{
                    "You need to specify a name!"
            });
        }
    }

    @Command(name = "mineme.info", aliases = {"mm.info", "mrl.info", "mine.info", "mines.info"}, description = "Shows info about a Mine", permission = "mineme.admin")
    public void info(CommandArgs args) {
        CommandSender p = args.getSender();
        if (args.length() > 0) {
            String name = args.getArgs(0);
            Mine m = mineManager.getMine(name);
            if (m != null) {
                mineManager.sendInfo(p, m);
            } else {
                messageManager.sendMessage(p, "Could not find mine " + name + ".");
                listMines(p);
            }
        } else {
            messageManager.sendMessage(p, "You need to specify a name!");
        }
    }

    @Command(name = "mineme.list", aliases = {"mm.list", "mrl.list", "mine.list", "mines.list"}, description = "Lists all the mines", permission = "mineme.admin")
    public void list(CommandArgs args) {
        listMines(args.getSender());
    }

    @Command(name = "mineme.gui", aliases = {"mm.gui", "mrl.gui", "mine.gui", "mines.gui"}, description = "Opens MEGUI", inGameOnly = true, permission = "mineme.admin")
    public void gui(CommandArgs args) {
        plugin.getGUIManager().getMainMenu().open(args.getPlayer());
    }

    @Command(name = "mineme.help", aliases = {"mm.help", "mrl.help", "mine.help", "mines.help"}, description = "Show the help", permission = "mineme.admin")
    public void help(CommandArgs args) {
        messageManager.sendMessage(args.getSender(), MessageColor.ERROR + "The help is a lie! " + MessageColor.PRIMARY + "Use /mineme");
    }

    @Command(name = "mineme.reload", aliases = {"mm.reload", "mrl.reload", "mine.reload", "mines.reload"}, description = "Show the help", permission = "mineme.admin")
    public void reload(CommandArgs args) {
        plugin.reload(args.getSender());
    }

    public void listMines(CommandSender p) {
        String s = "";
        List<Mine> mines = mineManager.getMines();
        for (Mine m : mines) {
            s = s.concat(messageManager.translateAll(m.getName(), m));
            if (mines.indexOf(m) != mines.size() - 1) {
                s = s.concat(MessageColor.SECONDARY + ", " + MessageColor.PRIMARY);
            }
        }
        messageManager.sendMessage(p, MessageColor.SECONDARY + "Available mines: " + MessageColor.PRIMARY + "" + s);
    }

    public void createNewMine(Player p, String name, Selection selection) {
        if (!p.hasPermission("mineme.admin")) {
            messageManager.sendMessage(p, "You do not have permission to perform that action");
            return;
        }
        Mine<?> m = null;
        messageManager.sendMessage(p, messageManager.translateAll("$2WorldEdit selection type: $4" + selection.getClass().getSimpleName()));
        if (selection instanceof CylinderSelection) {
            CylinderSelection cs = (CylinderSelection) selection;
            Location center = new Location(p.getWorld(), cs.getCenter().getX(), cs.getMinimumPoint().getBlockY(), cs.getCenter().getZ());
            int height = cs.getMaximumPoint().getBlockY() - cs.getMinimumPoint().getBlockY();
            m = new CircularMine(plugin, name, center, cs.getRadius().getX(), height);
        } else if (selection instanceof CuboidSelection) {
            CuboidSelection cuboidSelection = (CuboidSelection) selection;
            Location loc1 = cuboidSelection.getMinimumPoint();
            Location loc2 = cuboidSelection.getMaximumPoint();
            Location fl1 = loc1.clone();
            Location fl2 = loc2.clone();
            fl1.setX(Math.min(loc1.getBlockX(), loc2.getBlockX()));
            fl1.setY(Math.min(loc1.getBlockY(), loc2.getBlockY()));
            fl1.setZ(Math.min(loc1.getBlockZ(), loc2.getBlockZ()));
            fl2.setX(Math.max(loc1.getBlockX(), loc2.getBlockX()));
            fl2.setY(Math.max(loc1.getBlockY(), loc2.getBlockY()));
            fl2.setZ(Math.max(loc1.getBlockZ(), loc2.getBlockZ()));
            m = new CuboidMine(plugin, name, fl1, fl2);
        } else if (selection instanceof Polygonal2DSelection) {
            Polygonal2DSelection polySel = (Polygonal2DSelection) selection;
            m = new PolygonMine(plugin, polySel, name, p.getLocation().getWorld());
        }
        if (m == null) {
            messageManager.sendMessage(p, "$4Selection type $1" + selection.getClass().getSimpleName() + "$4 isn't supported! (Yet :D)");
            plugin.printException(name, new UnsupportedWorldEditRegionType(selection));
            return;
        }
        for (Block m1 : m) {
            if (mineManager.isPartOfMine(m1)) {
                messageManager.sendMessage(p, "It seems that there is already a mine here! Please try somewhere else :D");
                return;
            }
        }
        mineManager.registerMine(m);
        messageManager.sendMessage(p,
                messageManager.translateAll(plugin.getConfigurationManager().mineCreateMessage(), m));
        m.save();
        if (plugin.getConfigurationManager().useHolograms()) {
            if (m instanceof HologramCompatibleMine) {
                HologramCompatibleMine h = (HologramCompatibleMine) m;
                h.setupHolograms();
            }
        }
        m.reset();
    }
}
